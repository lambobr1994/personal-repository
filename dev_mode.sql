explore: lms_mtd_with_unicorn_pdt {
  fields: [ALL_FIELDS*, -core_order.booking_type,-core_order.booking_entity,-renewal_order.booking_type,-renewal_order.booking_entity ]
  # persist_for: "6 hours"

  join: core_order {
    type: left_outer
    relationship: one_to_one
    sql_on:  case when ${lms_mtd_with_unicorn_pdt.data_source} in ('B2C','LMS_licenses','LMS_credits')
                  then  ${core_order.id}
                  else  ${core_order.tmp_unicorn_purchase_id}  END
                  = ${lms_mtd_with_unicorn_pdt.order_id} ;;
  }

  join: v_users {
    relationship: one_to_one
    sql_on: ${core_order.user_id} = ${v_users.id} ;;
  }

  join: order_type {
    relationship: one_to_one
    sql_on: ${core_order.order_type}  =${order_type.id}  ;;
  }

  join: renewal_order{
    from:  core_order
    relationship: one_to_one
    type:  left_outer
    sql_on:  ${core_order.id} = ${renewal_order.parent_order_id}  ;;
  }

  join: renewal_order_status{
    from:  order_status
    relationship: one_to_one
    type:  left_outer
    sql_on:  ${renewal_order.order_status} = ${renewal_order_status.id}  ;;
  }

  join: renewal_core_orderproduct{
    from: core_orderproduct
    relationship: one_to_one
    type:  left_outer
    sql_on:  ${renewal_order.id} = ${renewal_core_orderproduct.order_id}  ;;
  }

  join: core_latest_payment_by_order_id_pdt {
    relationship: one_to_one
    sql_on: ${core_order.id} = ${core_latest_payment_by_order_id_pdt.order_id}  ;;
  }

  join: latest_payment {
    from:  core_payment
    relationship: one_to_one
    sql_on: ${core_latest_payment_by_order_id_pdt.latest_payment_id}  = ${latest_payment.id} ;;
  }

  join: latest_payment_status {
    from: payment_status
    relationship: one_to_one
    sql_on: ${latest_payment.payment_status} = ${latest_payment_status.id}  ;;
  }

  join: first_paid_order_pdt {
    type: left_outer
    relationship: one_to_one
    sql_on: ${lms_mtd_with_unicorn_pdt.order_id} = ${first_paid_order_pdt.first_paid_order_id} and ${lms_mtd_with_unicorn_pdt.data_source} in ('B2C','LMS_licenses','LMS_credits') ;;
  }

  join: accounts {
    type: left_outer
    relationship: one_to_one
    sql_on: ${lms_mtd_with_unicorn_pdt.account_id} = ${accounts.id}   ;;
  }

  join: core_productvariant {
    type: left_outer
    relationship: one_to_one
    sql_on: ${core_orderproduct.product_variant_id} = ${core_productvariant.id}   ;;
  }

  join: core_product {
    type: left_outer
    relationship: one_to_one
    sql_on: ${core_productvariant.product_id} = ${core_product.id}   ;;
  }

  join: core_orderproduct {
    type: left_outer
    relationship: one_to_one
    sql_on: ${core_order.id} = ${core_orderproduct.order_id}   ;;
  }

  join: core_examattempt_by_orderproduct_pdt {
    relationship: one_to_one
    sql_on: ${core_orderproduct.id} = ${core_examattempt_by_orderproduct_pdt.orderproduct_id} ;;
  }

  join: core_usercontentaccessthroughorders {
    relationship: one_to_one
    sql_on: ${core_orderproduct.id} = ${core_usercontentaccessthroughorders.order_product_id}  ;;
  }

  join: core_usercontentaccess {
    relationship: one_to_one
    sql_on: ${core_usercontentaccessthroughorders.user_content_access_id} = ${core_usercontentaccess.id} ;;
  }

  # join: core_examattempt {
  #   relationship: one_to_one
  #   sql_on: ${core_usercontentaccessthroughorders.user_content_access_id} = ${core_examattempt.usercontentaccess_ptr_id} ;;
  # }

  # join: examattempt_status {
  #   type: left_outer
  #   relationship: one_to_one
  #   sql_on: ${core_examattempt.status} = ${examattempt_status.id}   ;;
  # }

  join: core_subscriptioncourseselection {
    relationship: one_to_one
    sql_on: ${core_usercontentaccessthroughorders.user_content_access_id} = ${core_subscriptioncourseselection.user_content_access_id}  ;;
  }

  join: course_selected {
    from:  core_product
    relationship: one_to_one
    sql_on: ${core_subscriptioncourseselection.product_id} = ${course_selected.id}  ;;
  }

  join: unicorn_purchases {
    type: left_outer
    relationship: one_to_one
    sql_on: ${lms_mtd_with_unicorn_pdt.order_id} = ${unicorn_purchases.id}  AND ${lms_mtd_with_unicorn_pdt.data_source} in ('unicorn_platform','unicorn_salesforce') ;;
  }

  join: unicorn_courses {
    type: left_outer
    relationship: many_to_one
    sql_on: ${unicorn_purchases.course_id} = ${unicorn_courses.id}  ;;
  }

  join: unicorn_lab {
    type: left_outer
    relationship: one_to_one
    sql_on: ${unicorn_lab.purchase_id} = ${unicorn_purchases.id}  ;;
  }

  join: root_learningunit {
    from:  core_learningunit
    relationship: one_to_one
    sql_on:  COALESCE(${course_selected.name},${core_product.name}) = ${root_learningunit.name}  ;;
  }

  join: lum_section {
    from: core_learningunitmap
    relationship: one_to_one
    sql_on: ${lum_section.top_level_learning_unit_id} =${root_learningunit.id}  ;;
  }

  join: core_progressaggregate {
    relationship: one_to_one
    sql_on: ${v_users.id} = ${core_progressaggregate.user_id} AND ${lum_section.secondary_learning_unit_id} = ${core_progressaggregate.learning_path_id} and ${lum_section.relation_type} = 'hierarchical' ;;
  }

  join: total_learningunits_pdt {
    relationship: one_to_one
    sql_on: ${root_learningunit.id} = ${total_learningunits_pdt.course_id}  ;;
  }
}

----------------------------------------------------------------------------------------------------------------------------------------------

view: core_examattempt_aggregate_by_orderproduct_pdt {
  # Or, you could make this view a derived table, like this:
  derived_table: {
    sql: SELECT
            core_orderproduct.id AS orderproduct_id
            , CASE WHEN core_examattempt.exam_type = "awae" THEN "WEB-300"
              WHEN core_examattempt.exam_type = "ctp" THEN "CTP"
              WHEN core_examattempt.exam_type = "etbd"  THEN "PEN-300"
              WHEN core_examattempt.exam_type = "exp312"  THEN "EXP-312"
              WHEN core_examattempt.exam_type = "klr" THEN "KLCP"
              WHEN core_examattempt.exam_type = "pwk" THEN "PEN-200"
              WHEN core_examattempt.exam_type = "soc200"  THEN "SOC-200"
              WHEN core_examattempt.exam_type = "web200"  THEN "WEB-200"
              WHEN core_examattempt.exam_type = "wifu"  THEN "PEN-210"
              WHEN core_examattempt.exam_type = "wumed" THEN "EXP-301"
              ELSE core_examattempt.exam_type END AS course
            , COUNT(DISTINCT case when core_examattempt.status in (13) then usercontentaccess_ptr_id END) AS count_passed
            , COUNT(DISTINCT case when core_examattempt.status in (13, 14, 16, 1, 9) then usercontentaccess_ptr_id END) AS count_attempts
            , COUNT(DISTINCT case when core_examattempt.status in (0,3) then usercontentaccess_ptr_id END) AS count_scheduled
            , max(case when status in (13) then starts_at END) AS passed_date
            , max(case when status in (14,16) then starts_at END) AS failed_date
            , max(case when status in (0,3) then starts_at END) AS scheduled_date
        FROM `offsec_platform`.`core_examattempt` AS `core_examattempt`
        LEFT JOIN `analytics`.`examattempt_status` AS `examattempt_status` ON `core_examattempt`.`status` = `examattempt_status`.`id`
        LEFT JOIN `offsec_platform`.`core_usercontentaccessthroughorders` AS `core_usercontentaccessthroughorders` ON `core_examattempt`.`usercontentaccess_ptr_id` = `core_usercontentaccessthroughorders`.`user_content_access_id`
        LEFT JOIN `offsec_platform`.`core_orderproduct` AS `core_orderproduct` ON `core_usercontentaccessthroughorders`.`order_product_id` = `core_orderproduct`.`id`
        GROUP BY 1,2

      ;;
  }

  dimension: orderproduct_id {
    type: number
    sql: ${TABLE}.orderproduct_id ;;
  }

  dimension: course {
    type: number
    sql: ${TABLE}.course ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }

  # status codes equivalent in examattempt_status table:
  #   0 = rescheduled; 1 = cancelled; 3 = scheduled; 9 = revoked; 13 = passed; 14 = failed; 16 = failed pre-exam

  dimension: count_passed {
    label: "#Passed"
    type: number
    sql: ${TABLE}.count_passed ;;
  }

  dimension: count_attempts {
    label: "#Attempts"
    type: number
    sql: ${TABLE}.count_attempts ;;
  }

  dimension: count_scheduled {
    label: "#Scheduled"
    type: number
    sql: ${TABLE}.count_scheduled ;;
  }

  dimension: count_scheduled {
    label: "#Scheduled"
    type: number
    sql: ${TABLE}.count_scheduled ;;
  }


  measure: count_passed {
    label: "#Passed"
    type: count_distinct
    sql: case when ${TABLE}.status in (13) then ${TABLE}.usercontentaccess_ptr_id END ;;
  }

  measure: count_attempts {
    label: "#Attempts"
    type:  count_distinct
    sql:  case when ${TABLE}.status in (13, 14, 16, 1, 9) then ${TABLE}.usercontentaccess_ptr_id END ;;
  }

  measure: exam_scheduled {
    label: "#Scheduled"
    type:  count_distinct
    sql:  case when ${TABLE}.status in (0, 3) then ${TABLE}.usercontentaccess_ptr_id END ;;
  }

  measure: date_passed {
    type: string
    sql: max(case when ${TABLE}.status in (13) then ${TABLE}.starts_at END)  ;;
  }

  measure: date_failed {
    type: string
    sql: max(case when ${TABLE}.status in (14,16) then ${TABLE}.starts_at END) ;;
  }

  measure: date_scheduled {
    type: string
    sql: max(case when ${TABLE}.status in (0,3) then ${TABLE}.starts_at END) ;;
  }



  # measure: users_passed {
  #   label: "#Users Passed"
  #   type: count_distinct
  #   sql: case when ${TABLE}.status in (13) then ${v_users.id} END ;;
  # }

  # measure: users_attempted {
  #   label: "#Users Attempted"
  #   type: count_distinct
  #   sql: case when ${TABLE}.status in (13, 14, 16, 1, 9) then ${v_users.id} END ;;
  # }

  # measure: users_scheduled {
  #   label: "#Users Scheduled"
  #   type:  count_distinct
  #   sql:  case when ${TABLE}.status in (0, 3) then ${v_users.id} END ;;
  # }

}


----------------------------------------------------------------------------------------------------------------------------------------------
  measure: passed_date {
    type: string
    sql: MAX(case when ${TABLE}.status in (13) then ${starts_date} end) ;;
  }

  measure: next_exam_date {
    type: string
    sql: MAX(case when ${TABLE}.status in (0,3) then ${starts_date} end) ;;
  }

  measure: last_exam_attempt_date {
    type: string
    sql: MAX(CASE WHEN ${TABLE}.status not in (0, 3) THEN ${starts_date} END)  ;;
  }

  measure: last_exam_result {
    type: string
    sql: SUBSTRING_INDEX(GROUP_CONCAT(CASE WHEN ${starts_date} IS NOT NULL AND ${TABLE}.status not in (0, 3) THEN ${examattempt_status.examattempt_status_name} END ORDER BY starts_at DESC),',',1)  ;;
  }


  ---------------------------
  --------------------------

  view: core_examattempt_by_orderproduct_pdt {
  # Or, you could make this view a derived table, like this:
  derived_table: {
    sql:
     SELECT
            core_orderproduct.id AS orderproduct_id
            , CASE WHEN core_examattempt.exam_type = "awae" THEN "WEB-300"
              WHEN core_examattempt.exam_type = "ctp" THEN "CTP"
              WHEN core_examattempt.exam_type = "etbd"  THEN "PEN-300"
              WHEN core_examattempt.exam_type = "exp312"  THEN "EXP-312"
              WHEN core_examattempt.exam_type = "klr" THEN "KLCP"
              WHEN core_examattempt.exam_type = "pwk" THEN "PEN-200"
              WHEN core_examattempt.exam_type = "soc200"  THEN "SOC-200"
              WHEN core_examattempt.exam_type = "web200"  THEN "WEB-200"
              WHEN core_examattempt.exam_type = "wifu"  THEN "PEN-210"
              WHEN core_examattempt.exam_type = "wumed" THEN "EXP-301"
          ELSE core_examattempt.exam_type END AS course
            -- , COUNT(DISTINCT case when core_examattempt.status in (13) then usercontentaccess_ptr_id END) AS count_passed
            , COUNT(DISTINCT case when core_examattempt.status in (13, 14, 16, 1, 9) then usercontentaccess_ptr_id END) AS count_attempts
            -- , COUNT(DISTINCT case when core_examattempt.status in (0,3) then usercontentaccess_ptr_id END) AS count_scheduled
            , max(case when status in (13) then starts_at END) AS passed_date
            -- , max(case when status in (14,16) then starts_at END) AS failed_date
            , max(case when status in (0,3) then starts_at END) AS scheduled_date
            , MAX(CASE WHEN examattempt_status_name <> 'Scheduled' THEN starts_at END) AS last_exam_attempt_date
            , SUBSTRING_INDEX(GROUP_CONCAT(CASE WHEN starts_at IS NOT NULL AND examattempt_status_name <> 'Scheduled' THEN examattempt_status_name END ORDER BY starts_at DESC),',',1)   AS  last_exam_result
        FROM `offsec_platform`.`core_examattempt` AS `core_examattempt`
        LEFT JOIN `analytics`.`examattempt_status` AS `examattempt_status` ON `core_examattempt`.`status` = `examattempt_status`.`id`
        LEFT JOIN `offsec_platform`.`core_usercontentaccessthroughorders` AS `core_usercontentaccessthroughorders` ON `core_examattempt`.`usercontentaccess_ptr_id` = `core_usercontentaccessthroughorders`.`user_content_access_id`
        LEFT JOIN `offsec_platform`.`core_orderproduct` AS `core_orderproduct` ON `core_usercontentaccessthroughorders`.`order_product_id` = `core_orderproduct`.`id`
        GROUP BY 1,2

      ;;
  }

  # Define your dimensions and measures here, like this:
  dimension: orderproduct_id {
    type: number
    sql: ${TABLE}.orderproduct_id ;;
  }

  dimension: course {
    type: string
    sql: ${TABLE}.course ;;
  }

  dimension: passed_date{
    type: date
    sql: ${TABLE}.passed_date ;;
  }

  dimension: scheduled_date{
    type: date
    sql: ${TABLE}.scheduled_date ;;
  }

  dimension: last_exam_attempt_date{
    type: date
    sql: ${TABLE}.last_exam_attempt_date ;;
  }

  dimension: last_exam_attempt_result{
    type: date
    sql: ${TABLE}.last_exam_attempt_result ;;
  }

  measure: count_attempts {
    type: sum
    sql: ${TABLE}.count_attempts ;;
  }


}

