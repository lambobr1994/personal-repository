explore: core_usercontentaccess_lf_allocated {
  join: v_users {
    relationship: one_to_one
    type: left_outer
    sql_on: ${core_usercontentaccess_lf_allocated.user_id} =${v_users.id};;
  }


  join: core_learningunit {
    relationship: one_to_one
    type: left_outer
    sql_on: ${core_usercontentaccess_lf_allocated.lf_courses_learning_unit_id} =${core_learningunit.id} ;;
  }

  join: root_learningunit {
    from:  core_learningunit
    relationship: one_to_one
    type: left_outer
    sql_on: ${core_usercontentaccess_lf_allocated.learning_unit_id} =${root_learningunit.id} ;;
  }

  join: core_activitylog {
    relationship: one_to_one
    type: left_outer
    sql_on: ${core_learningunit.id} = ${core_activitylog.root_learning_path_id} AND ${core_usercontentaccess_lf_allocated.user_id} = ${core_activitylog.user_id} AND ${core_activitylog.timestamp_raw} BETWEEN ${core_usercontentaccess_lf_allocated.start_raw} AND ${core_usercontentaccess_lf_allocated.end_raw} ;;
  }

  join: activitylog_type {
    relationship: one_to_one
    sql_on: ${core_activitylog.activity_type} =${activitylog_type.id}   ;;
  }

  join: core_learningunitmap {
    relationship: one_to_one
    sql_on:  ${core_learningunitmap.top_level_learning_unit_id} = ${core_learningunit.id}  ;;
  }

  join: topic_learningunit {
    from: core_learningunit
    relationship: one_to_one
    sql_on:  ${core_learningunitmap.secondary_learning_unit_id} = ${topic_learningunit.id}  ;;
  }

  join: core_usersubmission {
    relationship: one_to_one
    sql_on: ${topic_learningunit.id} = ${core_usersubmission.learning_unit_id} AND ${core_usercontentaccess_lf_allocated.user_id} = ${core_usersubmission.user_id} AND ${core_usersubmission.created_raw} BETWEEN ${core_usercontentaccess_lf_allocated.start_raw} AND ${core_usercontentaccess_lf_allocated.end_raw} ;;
  }

  # questions
  join: question {
    from: core_learningunit
    type: inner
    relationship: one_to_one
    sql_on: ${question.id} =${core_usersubmission.learning_unit_id}    ;;
    view_label: ""
  }

  join: question_map{
    from: core_learningunitmap
    relationship: one_to_one
    type:  inner
    sql_on:  ${question.id} =${question_map.secondary_learning_unit_id}  and ${question.learning_unit_type} = 'question' and ${question_map.relation_type} = "hierarchical"   ;;
    view_label: ""
  }

  join: subsection_map{
    from: core_learningunitmap
    relationship: one_to_one
    type:  inner
    sql_on:  ${question_map.primary_learning_unit_id} = ${subsection_map.secondary_learning_unit_id} and ${question_map.relation_type} = "hierarchical" ;;
    view_label: ""
  }

  join: section_map{
    from: core_learningunitmap
    relationship: one_to_one
    type:  inner
    sql_on:  ${subsection_map.primary_learning_unit_id} = ${section_map.secondary_learning_unit_id} and ${subsection_map.relation_type} = "hierarchical" ;;
    view_label: ""
  }

  join: book_map{
    from: core_learningunitmap
    relationship: one_to_one
    type:  inner
    sql_on: ${section_map.primary_learning_unit_id} = ${book_map.secondary_learning_unit_id} and ${section_map.relation_type} = "hierarchical" ;;
    view_label: ""
  }

  join: question_module{
    from: core_learningunit
    relationship: one_to_one
    type:  inner
    sql_on: ${question_module.id} =${book_map.secondary_learning_unit_id} and   ${question_module.learning_unit_type} = 'book_module' and  ${question_module.status} = "published" and  ${book_map.relation_type} = "hierarchical" ;;
  }

  #books (read) and video (view)
  join: subsection_books_video{
    from: core_learningunit
    relationship: one_to_one
    type:  inner
    sql_on:  ${subsection_books_video.id} =${core_activitylog.learning_unit_id}  and ${core_activitylog.activity_type} IN (8,9)  ;;
    view_label: ""
  }

  join: subsection_map_books_video{
    from: core_learningunitmap
    relationship: one_to_one
    type:  inner
    sql_on:  ${subsection_books_video.id} =${subsection_map_books_video.secondary_learning_unit_id}  and ${subsection_books_video.learning_unit_type} IN ('subsection','video') and  ${subsection_books_video.status} = "published"  ;;
    view_label: ""
  }

  join: section_map_books_video{
    from: core_learningunitmap
    relationship: one_to_one
    type:  inner
    sql_on:   ${subsection_map_books_video.primary_learning_unit_id} = ${section_map_books_video.secondary_learning_unit_id} and ${subsection_map_books_video.relation_type} = "hierarchical"   ;;
    view_label: ""
  }

  join: book_map_books_video{
    from: core_learningunitmap
    relationship: one_to_one
    type:  inner
    sql_on:  ${section_map_books_video.primary_learning_unit_id} = ${book_map_books_video.secondary_learning_unit_id} and ${section_map_books_video.relation_type} = "hierarchical"  ;;
    view_label: ""
  }

  join: book_module{
    from: core_learningunit
    relationship: one_to_one
    type:  inner
    sql_on:  ${book_module.id} =${book_map_books_video.secondary_learning_unit_id} and   ${book_module.learning_unit_type} = 'book_module' and  ${book_module.status} = "published" and  ${book_map_books_video.relation_type} = "hierarchical"  ;;
  }

  join: video_module{
    from: core_learningunit
    relationship: one_to_one
    type:  inner
    sql_on:  ${video_module.id} =${book_map_books_video.secondary_learning_unit_id} and   ${video_module.learning_unit_type} = 'video_module' and  ${video_module.status} = "published" and  ${book_map_books_video.relation_type} = "hierarchical"  ;;
  }
}
