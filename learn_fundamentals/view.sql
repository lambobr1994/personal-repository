view: core_usercontentaccess_lf_allocated {
  # Or, you could make this view a derived table, like this:
  derived_table: {
    sql:
    SELECT uca.*, table1.lf_courses_learning_unit_id
    FROM offsec_platform.core_usercontentaccess AS uca
    LEFT JOIN (SELECT 16303 AS lf_courses_learning_unit_id UNION SELECT 32752 UNION SELECT 32756 UNION SELECT 9545 UNION SELECT 16306) AS table1 ON uca.learning_unit_id = 36053
    -- WHERE learning_unit_id = 36053
    -- learning unit IDs: 16303 = pen100, 32752 = soc100, 32756 = web100, 36053 = Learn Fundamentals, 9545 = pen210, 16306 = pen103
      ;;
  }

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: created_by_id {
    type: number
    sql: ${TABLE}.created_by_id ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.end ;;
  }

  dimension_group: expiration_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: date_add(date(now()), interval -90 day ) ;;
  }

  dimension: learning_unit_id {
    type: number
    sql: ${TABLE}.learning_unit_id ;;
  }

  dimension: parent_id {
    type: number
    sql: ${TABLE}.parent_id ;;
  }

  dimension: lf_courses_learning_unit_id {
    type: number
    sql: ${TABLE}.lf_courses_learning_unit_id ;;
  }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.start ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }

  dimension: updated_by_id {
    type: number
    sql: ${TABLE}.updated_by_id ;;
  }

  dimension: user_id {
    type: number
    sql: ${TABLE}.user_id ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }

  measure: count_userid {
    type: count_distinct
    value_format_name: decimal_0
    sql: ${TABLE}.user_id  ;;
  }
}
