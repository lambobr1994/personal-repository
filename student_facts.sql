WITH id AS
(
  SELECT 
    v_users.id AS user_id, v_users.os_id, v_users.first_name, v_users.last_name, v_users.email, v_users.country_name -- , CAST(v_users.date_joined AS DATE) AS date_joined
  FROM offsec_platform.v_users  AS v_users 
  WHERE 
    -- remove offsec employees
    v_users.email NOT LIKE '%evozon%' AND v_users.email NOT LIKE '%offensive-security.com%' AND v_users.email NOT LIKE '%accelone%'   -- AND os_id = 8416  --   AND os_id = 87737 --  AND os_id = 4569 AND os_id = 81669
)

, b2b_licspend AS
(
  SELECT 
    id.user_id
    , SUM(AOL.price)                    AS total_B2B_licspend
    , MIN(CAST(AOL.created_at AS DATE)) AS fst_B2B_lic_spend_dt
  FROM id
  LEFT JOIN offsec_platform.core_order  AS core_order ON id.user_id = core_order.user_id
  LEFT JOIN offsec_platform.accountorderslicenses  AS AOL ON core_order.id  = AOL.order_id 
  WHERE AOL.is_paid = true
  GROUP BY 1
)

, b2b_flexspend AS
(
  SELECT 
    id.user_id
    , SUM(AOC.price)                      AS total_B2B_flexspend
    , MIN(CAST(AOC.created_at  AS DATE))  AS fst_B2B_flex_spend_dt
  FROM id
  LEFT JOIN offsec_platform.core_order  AS core_order ON id.user_id = core_order.user_id
  LEFT JOIN offsec_platform.accountorderscredits  AS AOC ON core_order.id  = AOC.order_id
  WHERE AOC.is_paid = true
  GROUP BY 1
)

, b2b_redeems AS
(
  SELECT 
    id.user_id
    , invitations.id
    , invitations.action
    , CAST(invitations.redeemed_at AS DATE)             AS redeemed_dt
    , core_product.name                                 AS product_name
    , CASE WHEN core_product.name IN ('Lab Extension','Lab Access') THEN core_productvariant.code
           WHEN core_product.name IN ('PEN-200', 'CTP', 'SOC-100', 'PEN-300', 'WEB-300', 'EXP-301', 'EXP-312') THEN CONCAT(core_product.name,'_',core_productvariant.name) 
           ELSE core_product.name END                   AS product_variant
    , MIN(CAST(core_usercontentaccess.start AS DATE))   AS uca_dt      -- get only uca date of course itself.. exam uca date can be later
    , course_selected.name                              AS L1_course
    
  FROM id
  -- b2b products
  LEFT JOIN offsec_platform.invitations AS invitations ON id.user_id = invitations.redeemed_by 
  LEFT JOIN offsec_platform.accountordersusers AS accountordersusers ON invitations.id = accountordersusers.invite_id
  LEFT JOIN offsec_platform.core_order AS core_order ON accountordersusers.order_id = core_order.id
  LEFT JOIN offsec_platform.core_orderproduct AS core_orderproduct ON core_order.id = core_orderproduct.order_id
  LEFT JOIN offsec_platform.core_productvariant  AS core_productvariant ON core_orderproduct.product_variant_id = core_productvariant.id
  LEFT JOIN offsec_platform.core_product  AS core_product ON core_productvariant.product_id  =core_product.id
  
  -- access dates
  LEFT JOIN offsec_platform.core_usercontentaccessthroughorders AS core_usercontentaccessthroughorders ON core_orderproduct.id = core_usercontentaccessthroughorders.order_product_id
  LEFT JOIN offsec_platform.core_usercontentaccess AS core_usercontentaccess ON core_usercontentaccessthroughorders.user_content_access_id  = core_usercontentaccess.id
  LEFT JOIN offsec_platform.core_subscriptioncourseselection AS core_subscriptioncourseselection ON core_usercontentaccessthroughorders.user_content_access_id = core_subscriptioncourseselection.user_content_access_id
  LEFT JOIN offsec_platform.core_product AS course_selected ON core_subscriptioncourseselection.product_id = course_selected.id
  WHERE invitations.cancelled_at IS NULL AND core_product.name IS NOT NULL
  GROUP BY 1,2,3,4,5,6
)

, b2b_facts AS
(
  SELECT
    user_id
    , SUM(json_extract(action, '$.price'))                                                        AS total_B2B_redeem
    , SUM(CASE WHEN json_extract(action, '$.type') = 1 THEN json_extract(action, '$.price') END)  AS total_B2B_flexredeem
    , SUM(CASE WHEN json_extract(action, '$.type') = 2 THEN json_extract(action, '$.price') END)  AS total_B2B_licredeem
    , COUNT(DISTINCT CASE WHEN json_extract(action, '$.type') IN (1,2) THEN id END)               AS total_B2B_redeem_count
    , COUNT(DISTINCT CASE WHEN json_extract(action, '$.type') = 1 THEN id END)                    AS total_B2B_flexredeem_count
    , COUNT(DISTINCT CASE WHEN json_extract(action, '$.type') = 2 THEN id END)                    AS total_B2B_licredeem_count
    , MIN(uca_dt)                                                                                 AS fst_B2B_access_dt
    , MIN(redeemed_dt)                                                                            AS fst_B2B_redeem_dt
    , GROUP_CONCAT(DISTINCT product_variant ORDER BY redeemed_dt)                                 AS products_redeemed
    , GROUP_CONCAT(DISTINCT (CASE WHEN product_name = 'Learn One' THEN L1_course END) ORDER BY redeemed_dt) AS L1_courses_redeemed
  FROM b2b_redeems
  GROUP BY 1
)

, fst_play_practice_dt AS
(
  SELECT 
    id.user_id
    , MIN(CASE WHEN core_learningunit.name = 'Play' THEN CAST(core_usercontentaccess.start AS DATE) END)      AS fst_play_access_dt
    , MIN(CASE WHEN core_learningunit.name = 'Practice' THEN CAST(core_usercontentaccess.start AS DATE) END)  AS fst_practice_access_dt
  FROM id
  LEFT JOIN offsec_platform.core_usercontentaccess AS core_usercontentaccess ON id.user_id = core_usercontentaccess.user_id
  LEFT JOIN offsec_platform.core_learningunit AS core_learningunit ON core_usercontentaccess.learning_unit_id = core_learningunit.id 
  WHERE core_learningunit.name IN ('Play', 'Practice')
  GROUP BY 1
)

, b2c_purchases AS 
(
  SELECT *, COUNT(*) OVER (partition by user_id, order_id) AS order_id_items -- will be used for logic whether to use core_order.total or price.value
  FROM
  (
      SELECT 
        id.user_id
        , core_order.id                                     AS order_id
        , core_product.name                                 AS product_name
        , CASE WHEN core_product.name IN ('Lab Extension','Lab Access') THEN core_productvariant.code
               WHEN core_product.name IN ('PEN-200', 'CTP', 'SOC-100', 'PEN-300', 'WEB-300', 'EXP-301', 'EXP-312') THEN CONCAT(core_product.name,'_',core_productvariant.name) 
               ELSE core_product.name END  AS product_variant
        , core_order.total
        , price.value      
        , CAST(core_order.order_due_date AS DATE)           AS order_due_dt
        , MIN(CAST(core_usercontentaccess.start AS DATE))   AS uca_dt
        , course_selected.name AS L1_course
      
      FROM id
      -- B2C purchases
      LEFT JOIN offsec_platform.core_order  AS core_order ON id.user_id = core_order.user_id
      LEFT JOIN offsec_platform.core_orderproduct  AS core_orderproduct ON core_order.id =core_orderproduct.order_id
      LEFT JOIN offsec_platform.core_productvariant  AS core_productvariant ON core_orderproduct.product_variant_id = core_productvariant.id
      LEFT JOIN offsec_platform.core_product  AS core_product ON core_productvariant.product_id  =core_product.id
      LEFT JOIN offsec_platform.core_price  AS price ON core_productvariant.price_id  =price.id
      LEFT JOIN analytics.order_status  AS order_status ON core_order.order_status  =order_status.id
      LEFT JOIN offsec_platform.accountorderslicenses  AS accountorderslicenses ON core_order.id  = accountorderslicenses.order_id
      LEFT JOIN offsec_platform.accountorderscredits  AS accountorderscredits ON core_order.id  = accountorderscredits.order_id
      LEFT JOIN offsec_platform.accounts  AS accounts ON case when core_product.name = "Flex" then accountorderscredits.account_id  = accounts.id else  accountorderslicenses.account_id  = accounts.id end
      
      -- access dates
      LEFT JOIN offsec_platform.core_usercontentaccessthroughorders AS core_usercontentaccessthroughorders ON core_orderproduct.id = core_usercontentaccessthroughorders.order_product_id
      LEFT JOIN offsec_platform.core_usercontentaccess AS core_usercontentaccess ON core_usercontentaccessthroughorders.user_content_access_id  = core_usercontentaccess.id
      LEFT JOIN offsec_platform.core_subscriptioncourseselection AS core_subscriptioncourseselection ON core_usercontentaccessthroughorders.user_content_access_id = core_subscriptioncourseselection.user_content_access_id
      LEFT JOIN offsec_platform.core_product AS course_selected ON core_subscriptioncourseselection.product_id = course_selected.id
      WHERE core_order.total>0 AND order_status.order_status_name = 'Paid' AND accounts.account_name IS NULL AND core_product.name NOT IN ('Flex', 'PG Play', 'Kali Linux Revealed')
      GROUP BY 1,2,3,4,5,6,7
  ) AS t1
)

, b2c_facts AS 
(
  SELECT 
    user_id
    , SUM(CASE WHEN order_id_items>1 THEN value ELSE total END) AS total_B2C_spend
    , COUNT(DISTINCT order_id)                                        AS total_B2C_orders_count
    , MIN(uca_dt)                                               AS fst_B2C_access_dt
    , MIN(order_due_dt)                                         AS fst_B2C_paid_dt
    , GROUP_CONCAT(DISTINCT product_variant ORDER BY order_due_dt) AS products_purchased
    , GROUP_CONCAT(DISTINCT (CASE WHEN product_name = 'Learn One' THEN L1_course END) ORDER BY order_due_dt) AS L1_courses_purchased
  FROM b2c_purchases
  GROUP BY user_id
)

-- get the 1st and 2nd products purchased/redeemed
, products_order AS
(
  SELECT 
  user_id
  , SUBSTRING_INDEX(b2c.products_purchased,',',1)                                                                               AS 1st_product_purchased
  , CASE WHEN b2c.products_purchased LIKE '%,%' THEN SUBSTRING_INDEX(SUBSTRING_INDEX(b2c.products_purchased,',',2),',',-1) END  AS 2nd_product_purchased
  , SUBSTRING_INDEX(b2b.products_redeemed,',',1)                                                                                AS 1st_product_redeemed
  , CASE WHEN b2b.products_redeemed LIKE '%,%' THEN SUBSTRING_INDEX(SUBSTRING_INDEX(b2b.products_redeemed,',',2),',',-1) END    AS 2nd_product_redeemed
  FROM b2b_facts AS b2b
  JOIN b2c_facts AS b2c USING (user_id)
)

, engagement_lastmonth AS
(
  SELECT
    id.user_id
    , SUM(read_activity)                AS read_lastmonth
    , SUM(video_activity)               AS video_lastmonth
    , SUM(quession_submission_activity) AS questions_lastmonth
    , SUM(lab_submission_activity)      AS labs_lastmonth
  FROM id
  LEFT JOIN analytics.looker_activities_aggregate_monthly AS a ON id.user_id = a.user_id
  WHERE MONTH(`core_activitylog.timestamp_month`) = MONTH(NOW())-1
)

, engagement_ytd AS
(
  SELECT
    id.user_id
    , SUM(read_activity)                AS read_ytd
    , SUM(video_activity)               AS video_ytd
    , SUM(quession_submission_activity) AS questions_ytd
    , SUM(lab_submission_activity)      AS labs_ytd
  FROM id
  LEFT JOIN analytics.looker_activities_aggregate_monthly AS a ON id.user_id = a.user_id
  WHERE YEAR(`core_activitylog.timestamp_month`) = YEAR(NOW())
)

, engagement_last4weeks AS   -- last completed 4 weeks
(
  SELECT
    id.user_id
    , SUM(read_activity)                AS read_last4weeks
    , SUM(video_activity)               AS video_last4weeks
    , SUM(question_submission_activity) AS questions_last4weeks
    , SUM(lab_submission_activity)      AS labs_last4weeks
  FROM id
  LEFT JOIN analytics.looker_activities_aggregate_weekly AS a ON id.user_id = a.user_id
  WHERE WEEK(`core_activitylog.timestamp_week`) >= WEEK(DATE_SUB(NOW(),INTERVAL 4 WEEK)) AND WEEK(`core_activitylog.timestamp_week`) <> WEEK(NOW())
)

, exams_taken AS
(
  SELECT DISTINCT
    id.user_id
    , core_examattempt.usercontentaccess_ptr_id   AS exam_id
    , CASE WHEN core_examattempt.exam_type = "awae"  THEN "WEB-300"
          WHEN core_examattempt.exam_type = "ctp"    THEN "CTP"
          WHEN core_examattempt.exam_type = "etbd"   THEN "PEN-300"
          WHEN core_examattempt.exam_type = "exp312" THEN "EXP-312"
          WHEN core_examattempt.exam_type = "klr"    THEN "KLCP"
          WHEN core_examattempt.exam_type = "pwk"    THEN "PEN-200"
          WHEN core_examattempt.exam_type = "soc200" THEN "SOC-200"
          WHEN core_examattempt.exam_type = "web200" THEN "WEB-200"
          WHEN core_examattempt.exam_type = "wifu"   THEN "PEN-210"
          WHEN core_examattempt.exam_type = "wumed"  THEN "EXP-301"
      ELSE core_examattempt.exam_type END         AS product_name
    , CAST(core_examattempt.starts_at AS DATE)    AS exam_date
    , CAST(core_examattempt.scheduled_at AS DATE) AS scheduled_date
    , examattempt_status.examattempt_status_name  AS exam_status
  FROM id
  LEFT JOIN offsec_platform.core_usercontentaccess AS core_usercontentaccess ON core_usercontentaccess.user_id = id.user_id
  LEFT JOIN offsec_platform.core_examattempt AS core_examattempt   ON core_examattempt.usercontentaccess_ptr_id = core_usercontentaccess.id
  LEFT JOIN analytics.examattempt_status AS examattempt_status ON core_examattempt.status = examattempt_status.id
  WHERE CAST(core_examattempt.starts_at AS DATE) IS NOT NULL
)

, LU_courses AS
(
  SELECT 'PEN-200' AS LU_course UNION
  SELECT 'WEB-200' UNION
  SELECT 'SOC-200' UNION
  SELECT 'EXP-301' UNION
  SELECT 'EXP-312' UNION
  SELECT 'PEN-300' UNION
  SELECT 'WEB-300'
)

, all_products AS    
(
  SELECT 
    user_id
    , CASE WHEN product_name = 'Learn One' THEN L1_course ELSE product_name END AS product_name
    , uca_dt 
  FROM b2b_redeems
  
  UNION ALL
  
  SELECT 
    user_id
    , CASE WHEN product_name = 'Learn One' THEN L1_course ELSE product_name END AS product_name
    , uca_dt 
  FROM b2c_purchases
)

, courses_LU_allocated AS
(
  SELECT 
    user_id
    , CASE  WHEN product_name = 'Learn Unlimited' THEN LU_course
            ELSE product_name
      END AS product_name
    , uca_dt
  FROM
  (
      SELECT DISTINCT user_id, product_name, LU_course, uca_dt
      FROM all_products
      LEFT JOIN LU_courses ON all_products.product_name = 'Learn Unlimited' 
  ) AS t1
)

, exams_with_course_uca AS
(
  SELECT 
    *
    , ROW_NUMBER() OVER (PARTITION BY user_id, product_name ORDER BY (CASE WHEN exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') THEN 0 ELSE 1 END), exam_date) AS fst_exam_attempt
    , ROW_NUMBER() OVER (PARTITION BY user_id, product_name ORDER BY (CASE WHEN exam_status IN ('passed') THEN 0 ELSE 1 END), exam_date)                                                  AS fst_exam_passed
    
  FROM
  (
      SELECT 
        exams_taken.user_id
        , exams_taken.exam_id
        , exams_taken.product_name
        , exams_taken.exam_date
        , exams_taken.scheduled_date
        , exams_taken.exam_status
        , MIN(a.uca_dt) AS course_uca_dt
      FROM exams_taken
      LEFT JOIN courses_LU_allocated AS a ON exams_taken.user_id = a.user_id AND exams_taken.product_name = a.product_name
      GROUP BY 1,2,3,4,5,6
  ) AS t1
)

, exams_pivot AS
(
  SELECT
    user_id
    , GROUP_CONCAT(CASE WHEN exam_status='passed' THEN 
                                                    CASE WHEN product_name = 'PEN-200' THEN 'OSCP'
                                                         WHEN product_name = 'SOC-200' THEN 'OSDA'
                                                         WHEN product_name = 'WEB-200' THEN 'OSWA'
                                                         WHEN product_name = 'EXP-301' THEN 'OSED'
                                                         WHEN product_name = 'EXP-312' THEN 'OSMR'
                                                         WHEN product_name = 'PEN-300' THEN 'OSEP'
                                                         WHEN product_name = 'WEB-300' THEN 'OSWE'
                                                    END
                        END ORDER BY exam_date)                                                                                   AS passed_certs
                        
    , MIN(CASE WHEN product_name = 'PEN-200' THEN course_uca_dt END)                                                              AS PEN_200_fst_access_dt
    , MIN(CASE WHEN product_name = 'PEN-200' AND exam_status = 'passed' THEN exam_date END)                                       AS OSCP_fst_pass_dt
    , COUNT(DISTINCT CASE WHEN product_name = 'PEN-200' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') THEN exam_id END)                                                              AS OSCP_attempts
    , MAX(CASE WHEN product_name = 'PEN-200' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') AND fst_exam_attempt = 1 THEN DATEDIFF(exam_date, course_uca_dt) END)                     AS OSCP_days_course_start_to_fst_exam_attempt
    , GREATEST(MAX(CASE WHEN product_name = 'PEN-200' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') AND fst_exam_attempt = 1 THEN DATEDIFF(scheduled_date, course_uca_dt) END),0)    AS OSCP_days_course_start_to_fst_exam_scheduled
    , MAX(CASE WHEN product_name = 'PEN-200' AND exam_status IN ('passed') AND fst_exam_passed = 1 THEN DATEDIFF(exam_date, course_uca_dt) END)                                                                       AS OSCP_days_course_start_to_fst_exam_passed
    
    , MIN(CASE WHEN product_name = 'SOC-200' THEN course_uca_dt END)                                                              AS SOC_200_fst_access_dt
    , MIN(CASE WHEN product_name = 'SOC-200' AND exam_status = 'passed' THEN exam_date END)                                       AS OSDA_fst_pass_dt
    , COUNT(DISTINCT CASE WHEN product_name = 'SOC-200' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') THEN exam_id END)                                                              AS OSDA_attempts
    , MAX(CASE WHEN product_name = 'SOC-200' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') AND fst_exam_attempt = 1 THEN DATEDIFF(exam_date, course_uca_dt) END)                     AS OSDA_days_course_start_to_fst_exam_attempt
    , GREATEST(MAX(CASE WHEN product_name = 'SOC-200' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') AND fst_exam_attempt = 1 THEN DATEDIFF(scheduled_date, course_uca_dt) END),0)    AS OSDA_days_course_start_to_fst_exam_scheduled
    , MAX(CASE WHEN product_name = 'SOC-200' AND exam_status IN ('passed') AND fst_exam_passed = 1 THEN DATEDIFF(exam_date, course_uca_dt) END)                                                                       AS OSDA_days_course_start_to_fst_exam_passed
    
    , MIN(CASE WHEN product_name = 'WEB-200' THEN course_uca_dt END)                                                              AS WEB_200_fst_access_dt
    , MIN(CASE WHEN product_name = 'WEB-200' AND exam_status = 'passed' THEN exam_date END)                                       AS OSWA_fst_pass_dt
    , COUNT(DISTINCT CASE WHEN product_name = 'WEB-200' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') THEN exam_id END)                                                              AS OSWA_attempts
    , MAX(CASE WHEN product_name = 'WEB-200' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') AND fst_exam_attempt = 1 THEN DATEDIFF(exam_date, course_uca_dt) END)                     AS OSWA_days_course_start_to_fst_exam_attempt
    , GREATEST(MAX(CASE WHEN product_name = 'WEB-200' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') AND fst_exam_attempt = 1 THEN DATEDIFF(scheduled_date, course_uca_dt) END),0)    AS OSWA_days_course_start_to_fst_exam_scheduled
    , MAX(CASE WHEN product_name = 'WEB-200' AND exam_status IN ('passed') AND fst_exam_passed = 1 THEN DATEDIFF(exam_date, course_uca_dt) END)                                                                       AS OSWA_days_course_start_to_fst_exam_passed
    
    , MIN(CASE WHEN product_name = 'EXP-301' THEN course_uca_dt END)                                                              AS EXP_301_fst_access_dt
    , MIN(CASE WHEN product_name = 'EXP-301' AND exam_status = 'passed' THEN exam_date END)                                       AS OSED_fst_pass_dt
    , COUNT(DISTINCT CASE WHEN product_name = 'EXP-301' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') THEN exam_id END)                                                              AS OSED_attempts
    , MAX(CASE WHEN product_name = 'EXP-301' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') AND fst_exam_attempt = 1 THEN DATEDIFF(exam_date, course_uca_dt) END)                     AS OSED_days_course_start_to_fst_exam_attempt
    , GREATEST(MAX(CASE WHEN product_name = 'EXP-301' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') AND fst_exam_attempt = 1 THEN DATEDIFF(scheduled_date, course_uca_dt) END),0)    AS OSED_days_course_start_to_fst_exam_scheduled
    , MAX(CASE WHEN product_name = 'EXP-301' AND exam_status IN ('passed') AND fst_exam_passed = 1 THEN DATEDIFF(exam_date, course_uca_dt) END)                                                                       AS OSED_days_course_start_to_fst_exam_passed
     
    , MIN(CASE WHEN product_name = 'EXP-312' THEN course_uca_dt END)                                                              AS EXP_312_fst_access_dt
    , MIN(CASE WHEN product_name = 'EXP-312' AND exam_status = 'passed' THEN exam_date END)                                       AS OSMR_fst_pass_dt
    , COUNT(DISTINCT CASE WHEN product_name = 'EXP-312' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') THEN exam_id END)                                                              AS OSMR_attempts
    , MAX(CASE WHEN product_name = 'EXP-312' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') AND fst_exam_attempt = 1 THEN DATEDIFF(exam_date, course_uca_dt) END)                     AS OSMR_days_course_start_to_fst_exam_attempt
    , GREATEST(MAX(CASE WHEN product_name = 'EXP-312' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') AND fst_exam_attempt = 1 THEN DATEDIFF(scheduled_date, course_uca_dt) END),0)    AS OSMR_days_course_start_to_fst_exam_scheduled
    , MAX(CASE WHEN product_name = 'EXP-312' AND exam_status IN ('passed') AND fst_exam_passed = 1 THEN DATEDIFF(exam_date, course_uca_dt) END)                                                                       AS OSMR_days_course_start_to_fst_exam_passed
     
    , MIN(CASE WHEN product_name = 'PEN-300' THEN course_uca_dt END)                                                              AS PEN_300_fst_access_dt
    , MIN(CASE WHEN product_name = 'PEN-300' AND exam_status = 'passed' THEN exam_date END)                                       AS OSEP_fst_pass_dt
    , COUNT(DISTINCT CASE WHEN product_name = 'PEN-300' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') THEN exam_id END)                                                              AS OSEP_attempts
    , MAX(CASE WHEN product_name = 'PEN-300' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') AND fst_exam_attempt = 1 THEN DATEDIFF(exam_date, course_uca_dt) END)                     AS OSEP_days_course_start_to_fst_exam_attempt
    , GREATEST(MAX(CASE WHEN product_name = 'PEN-300' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') AND fst_exam_attempt = 1 THEN DATEDIFF(scheduled_date, course_uca_dt) END),0)    AS OSEP_days_course_start_to_fst_exam_scheduled
    , MAX(CASE WHEN product_name = 'PEN-300' AND exam_status IN ('passed') AND fst_exam_passed = 1 THEN DATEDIFF(exam_date, course_uca_dt) END)                                                                       AS OSEP_days_course_start_to_fst_exam_passed
    
    , MIN(CASE WHEN product_name = 'WEB-300' THEN course_uca_dt END)                                                              AS WEB_300_fst_access_dt
    , MIN(CASE WHEN product_name = 'WEB-300' AND exam_status = 'passed' THEN exam_date END)                                       AS OSWE_fst_pass_dt
    , COUNT(DISTINCT CASE WHEN product_name = 'WEB-300' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') THEN exam_id END)                                                              AS OSWE_attempts
    , MAX(CASE WHEN product_name = 'WEB-300' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') AND fst_exam_attempt = 1 THEN DATEDIFF(exam_date, course_uca_dt) END)                     AS OSWE_days_course_start_to_fst_exam_attempt
    , GREATEST(MAX(CASE WHEN product_name = 'WEB-300' AND exam_status IN ('passed','failed','failed_pre_exam','cancelled','revoked') AND fst_exam_attempt = 1 THEN DATEDIFF(scheduled_date, course_uca_dt) END),0)    AS OSWE_days_course_start_to_fst_exam_scheduled
    , MAX(CASE WHEN product_name = 'WEB-300' AND exam_status IN ('passed') AND fst_exam_passed = 1 THEN DATEDIFF(exam_date, course_uca_dt) END)                                                                       AS OSWE_days_course_start_to_fst_exam_passed
  FROM exams_with_course_uca
  GROUP BY 1
)


SELECT 
  id.*
  
  -- first join date
  , d.fst_play_access_dt
  , d.fst_practice_access_dt
  , e.fst_B2C_access_dt
  , c.fst_B2B_access_dt
  , e.fst_B2C_paid_dt
  , c.fst_B2B_redeem_dt
  , a.fst_B2B_lic_spend_dt
  , b.fst_B2B_flex_spend_dt
  
  -- spend
  , COALESCE(a.total_B2B_licspend,0) + COALESCE(b.total_B2B_flexspend,0) AS total_B2B_spend
  , a.total_B2B_licspend
  , b.total_B2B_flexspend
  , c.total_B2B_redeem
  , c.total_B2B_licredeem
  , c.total_B2B_flexredeem
  , e.total_B2C_spend
  , c.total_B2B_redeem_count
  , c.total_B2B_licredeem_count
  , c.total_B2B_flexredeem_count
  , e.total_B2C_orders_count
  
  -- products purchased or redeemed
  , e.products_purchased
  , c.products_redeemed
  , e.L1_courses_purchased
  , c.L1_courses_redeemed
  , f.1st_product_purchased
  , f.2nd_product_purchased
  , f.1st_product_redeemed
  , f.2nd_product_redeemed
  
  -- engagement
  , g.read_lastmonth
  , g.video_lastmonth
  , g.questions_lastmonth
  , g.labs_lastmonth
  , j.read_last4weeks
  , j.video_last4weeks
  , j.questions_last4weeks
  , j.labs_last4weeks
  , h.read_ytd
  , h.video_ytd
  , h.questions_ytd
  , h.labs_ytd
  
  -- exams
  , i.passed_certs
  , i.PEN_200_fst_access_dt
  , i.OSCP_fst_pass_dt
  , i.OSCP_attempts
  , i.OSCP_days_course_start_to_fst_exam_attempt
  , i.OSCP_days_course_start_to_fst_exam_scheduled
  , i.OSCP_days_course_start_to_fst_exam_passed
  , i.SOC_200_fst_access_dt
  , i.OSDA_fst_pass_dt
  , i.OSDA_attempts
  , i.OSDA_days_course_start_to_fst_exam_attempt
  , i.OSDA_days_course_start_to_fst_exam_scheduled
  , i.OSDA_days_course_start_to_fst_exam_passed
  , i.WEB_200_fst_access_dt
  , i.OSWA_fst_pass_dt
  , i.OSWA_attempts
  , i.OSWA_days_course_start_to_fst_exam_attempt
  , i.OSWA_days_course_start_to_fst_exam_scheduled
  , i.OSWA_days_course_start_to_fst_exam_passed
  , i.EXP_301_fst_access_dt
  , i.OSED_fst_pass_dt
  , i.OSED_attempts
  , i.OSED_days_course_start_to_fst_exam_attempt
  , i.OSED_days_course_start_to_fst_exam_scheduled
  , i.OSED_days_course_start_to_fst_exam_passed
  , i.EXP_312_fst_access_dt
  , i.OSMR_fst_pass_dt
  , i.OSMR_attempts
  , i.OSMR_days_course_start_to_fst_exam_attempt
  , i.OSMR_days_course_start_to_fst_exam_scheduled
  , i.OSMR_days_course_start_to_fst_exam_passed
  , i.PEN_300_fst_access_dt
  , i.OSEP_fst_pass_dt
  , i.OSEP_attempts
  , i.OSEP_days_course_start_to_fst_exam_attempt
  , i.OSEP_days_course_start_to_fst_exam_scheduled
  , i.OSEP_days_course_start_to_fst_exam_passed
  , i.WEB_300_fst_access_dt
  , i.OSWE_fst_pass_dt
  , i.OSWE_attempts
  , i.OSWE_days_course_start_to_fst_exam_attempt
  , i.OSWE_days_course_start_to_fst_exam_scheduled
  , i.OSWE_days_course_start_to_fst_exam_passed

FROM id
LEFT JOIN b2b_licspend          AS a ON id.user_id = a.user_id -- AND id.account_id = a.account_id
LEFT JOIN b2b_flexspend         AS b ON id.user_id = b.user_id -- AND id.account_id = b.account_id
LEFT JOIN b2b_facts             AS c ON id.user_id = c.user_id -- AND id.account_id = c.account_id
LEFT JOIN fst_play_practice_dt  AS d ON id.user_id = d.user_id
LEFT JOIN b2c_facts             AS e ON id.user_id = e.user_id
LEFT JOIN products_order        AS f ON id.user_id = f.user_id
LEFT JOIN engagement_lastmonth  AS g ON id.user_id = g.user_id
LEFT JOIN engagement_ytd        AS h ON id.user_id = h.user_id
LEFT JOIN exams_pivot           AS i ON id.user_id = i.user_id
LEFT JOIN engagement_last4weeks AS j ON id.user_id = j.user_id
LIMIT 100
