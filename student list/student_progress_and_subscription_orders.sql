WITH  core_latest_payment_by_order_product_id_pdt AS
(
  select order_product_id , order_id, max(case when core_payment.payment_status = 1 then core_payment.id end) as latest_payment_id
  from  offsec_platform.core_orderpayment  AS core_orderpayment
  LEFT JOIN offsec_platform.core_payment  AS core_payment ON core_orderpayment.payment_id  = core_payment.id
  group by 1
)

, sub_orders AS
(
  SELECT
      `v_users`.`os_id` AS os_id
      , `v_users`.`id` AS user_id
      , v_users.email AS email
      , (DATE(`v_users`.`last_login`)) AS last_login_date
      , accounts.account_name AS company_name
      , admin.os_id AS master_account_osid
      , `v_users`.`country_name` AS country
      , `core_order`.`id` AS order_id
      , invitations.id AS invite_id
      , `core_orderproduct`.`id` AS orderproduct_id
      ,    (DATE(`core_order`.`order_due_date`)) AS order_due_date
      , (CAST(core_payment.payment_date  AS CHAR(19))) AS payment_time
      , CASE WHEN `core_product`.`name` = 'Learn One' THEN 'L1' 
             WHEN `core_product`.`name` = 'Learn Unlimited' THEN 'LU' 
             WHEN `core_product`.`name` = 'Learn Fundamentals' THEN 'LF' 
        END AS purchase_type
      , `order_type`.`order_type_name` AS order_type
      , `order_status`.`order_status_name` AS order_status
      , CASE WHEN `core_order_b2b_b2c_tag`.`purchase_type` = 'b2b_redeem' THEN 'b2b' ELSE 'b2c' END AS student_type
      ,    (DATE(`core_usercontentaccess`.`start`)) AS access_start_date
      ,    (DATE(`core_usercontentaccess`.`end`)) AS access_end_date
      , `course_selected`.`name` AS course
  FROM
      `offsec_platform`.`core_order` AS `core_order`
      LEFT JOIN `offsec_platform`.`core_orderproduct` AS `core_orderproduct` ON `core_order`.`id` = `core_orderproduct`.`order_id`
      LEFT JOIN `offsec_platform`.`core_productvariant` AS `core_productvariant` ON `core_orderproduct`.`product_variant_id` = `core_productvariant`.`id`
      LEFT JOIN `offsec_platform`.`core_product` AS `core_product` ON `core_productvariant`.`product_id` = `core_product`.`id`
      LEFT JOIN `analytics`.`order_status` AS `order_status` ON `core_order`.`order_status` = `order_status`.`id`
      LEFT JOIN `analytics`.`order_type` AS `order_type` ON `core_order`.`order_type` = `order_type`.`id`
      LEFT JOIN `offsec_platform`.`core_usercontentaccessthroughorders` AS `core_usercontentaccessthroughorders` ON `core_orderproduct`.`id` = `core_usercontentaccessthroughorders`.`order_product_id`
      LEFT JOIN `offsec_platform`.`core_subscriptioncourseselection` AS `core_subscriptioncourseselection` ON `core_usercontentaccessthroughorders`.`user_content_access_id` = `core_subscriptioncourseselection`.`user_content_access_id`
      LEFT JOIN `offsec_platform`.`core_product` AS `course_selected` ON `core_subscriptioncourseselection`.`product_id` = `course_selected`.`id`
      LEFT JOIN `offsec_platform`.`core_usercontentaccess` AS `core_usercontentaccess` ON `core_usercontentaccessthroughorders`.`user_content_access_id` = `core_usercontentaccess`.`id`
      LEFT JOIN `offsec_platform`.`v_users` AS `v_users` ON `core_order`.`user_id` = `v_users`.`id`
      LEFT JOIN `analytics`.`looker_core_order_b2b_b2c_tag` AS `core_order_b2b_b2c_tag` ON `core_order`.`id` = `core_order_b2b_b2c_tag`.`order_id`
      LEFT JOIN accountordersusers ON core_order.id = accountordersusers.order_id
      LEFT JOIN accounts ON accountordersusers.account_id = accounts.id
      LEFT JOIN invitations ON accountordersusers.invite_id = invitations.id
      LEFT JOIN offsec_platform.v_users AS admin ON invitations.created_by = admin.id
      LEFT JOIN core_latest_payment_by_order_product_id_pdt ON core_orderproduct.id = core_latest_payment_by_order_product_id_pdt.order_product_id
      LEFT JOIN core_payment ON core_latest_payment_by_order_product_id_pdt.latest_payment_id  = core_payment.id
  WHERE `core_product`.`name` IN ('Learn Fundamentals', 'Learn One', 'Learn Unlimited') AND `core_order_b2b_b2c_tag`.`purchase_type` IN ('b2b_redeem', 'b2c') 
        AND v_users.email NOT LIKE '%evozon%' AND v_users.email NOT LIKE '%offensive-security.com%' AND v_users.email NOT LIKE '%accelone%'
  GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18
)

, order_price AS
(
  SELECT core_order.id AS order_id, CASE WHEN COUNT(*)=1 THEN total ELSE 'UNK' END AS price
  FROM core_order
  LEFT JOIN core_orderproduct ON core_order.id = core_orderproduct.order_id
  GROUP BY 1
)

, invitation_price AS 
(
  SELECT invitations.id AS invite_id, json_extract(action, '$.price') AS price
  FROM invitations
  GROUP BY 1,2
)

, sub_allocation AS
(
  -- manual allocation of LF and LU courses
  SELECT 'LF' AS sub, 'PEN-100' AS course UNION ALL
  SELECT 'LF', 'WEB-100' UNION ALL
  SELECT 'LF', 'SOC-100' UNION ALL
  SELECT 'LF', 'CLD-100' UNION ALL
  SELECT 'LF', 'PEN-103' UNION ALL
  SELECT 'LF', 'PEN-210' UNION ALL 
  SELECT 'LU', 'PEN-100' UNION ALL
  SELECT 'LU', 'WEB-100' UNION ALL
  SELECT 'LU', 'SOC-100' UNION ALL
  SELECT 'LU', 'CLD-100' UNION ALL
  SELECT 'LU', 'PEN-103' UNION ALL
  SELECT 'LU', 'PEN-210' UNION ALL 
  SELECT 'LU', 'PEN-200' UNION ALL 
  SELECT 'LU', 'WEB-200' UNION ALL 
  SELECT 'LU', 'SOC-200' UNION ALL 
  SELECT 'LU', 'PEN-300' UNION ALL 
  SELECT 'LU', 'WEB-300' UNION ALL 
  SELECT 'LU', 'EXP-301' UNION ALL 
  SELECT 'LU', 'EXP-312'
)

, allocated AS 
(
  SELECT a.os_id, a.user_id, a.email, a.last_login_date, a.company_name, a.master_account_osid, a.country, a.access_start_date, a.access_end_date, a.student_type, a.purchase_type
    , case when a.course IN ('LF','LU') THEN b.course ELSE a.course END AS course
    , a.orderproduct_id , a.order_type, a.order_status, a.order_id, a.order_due_date, a.invite_id, a.payment_time
  FROM sub_orders AS a
  LEFT JOIN sub_allocation AS b ON a.course = b.sub
)

, total_learningunits AS (
    with included_lu as
    (
      select lu_top.id AS course_id, lu_top.name as course_name, lu_top.code as course_code, lu_module.id as module_id, lu_module.learning_unit_type as module_learning_unit_type,
          lu1.id, lu1.name, lu1.learning_unit_type
      from core_learningunit lu_top
      join core_learningunitmap lum0 on lum0.top_level_learning_unit_id = lu_top.id and lum0.relation_type = 'hierarchical'
      join core_learningunit lu_module on lum0.secondary_learning_unit_id = lu_module.id and lu_module.learning_unit_type in ('book_module', 'video_module', 'lab')
      left join core_learningunitmap lum1 on lum1.primary_learning_unit_id = lu_module.id and lum1.relation_type = 'hierarchical'
      left join core_learningunit lu1 on lum1.secondary_learning_unit_id = lu1.id and lu1.status in ('published','archived')
          and (
              lu1.learning_unit_type in ('subsection', 'video', 'question')
              or lu1.learning_unit_type = 'host' and exists (select 1 from core_proof p where p.learning_unit_id = lu1.id)
              )

      union

      select lu_top.id AS course_id, lu_top.name as course_name, lu_top.code as course_code, lu_module.id as module_id, lu_module.learning_unit_type as module_learning_unit_type,
          lu2.id, lu2.name, lu2.learning_unit_type
      from core_learningunit lu_top
      join core_learningunitmap lum0 on lum0.top_level_learning_unit_id = lu_top.id and lum0.relation_type = 'hierarchical'
      join core_learningunit lu_module on lum0.secondary_learning_unit_id = lu_module.id and lu_module.learning_unit_type in ('book_module', 'video_module', 'lab')
      left join core_learningunitmap lum1 on lum1.primary_learning_unit_id = lu_module.id and lum1.relation_type = 'hierarchical'
      left join core_learningunit lu1 on lum1.secondary_learning_unit_id = lu1.id and lu1.status in ('published','archived')
      left join core_learningunitmap lum2 on lum2.primary_learning_unit_id = lu1.id and lum2.relation_type = 'hierarchical'
      left join core_learningunit lu2 on lum2.secondary_learning_unit_id = lu2.id and lu2.status in ('published','archived')
          and (
              lu2.learning_unit_type in ('subsection', 'video', 'question')
              or lu2.learning_unit_type = 'host' and exists (select 1 from core_proof p where p.learning_unit_id = lu2.id)
              )

      union

      select lu_top.id AS course_id, lu_top.name as course_name, lu_top.code as course_code, lu_module.id as module_id, lu_module.learning_unit_type as module_learning_unit_type,
          lu3.id, lu3.name, lu3.learning_unit_type
      from core_learningunit lu_top
      join core_learningunitmap lum0 on lum0.top_level_learning_unit_id = lu_top.id and lum0.relation_type = 'hierarchical'
      join core_learningunit lu_module on lum0.secondary_learning_unit_id = lu_module.id and lu_module.learning_unit_type in ('book_module', 'video_module', 'lab')
      left join core_learningunitmap lum1 on lum1.primary_learning_unit_id = lu_module.id and lum1.relation_type = 'hierarchical'
      left join core_learningunit lu1 on lum1.secondary_learning_unit_id = lu1.id and lu1.status in ('published','archived')
      left join core_learningunitmap lum2 on lum2.primary_learning_unit_id = lu1.id and lum2.relation_type = 'hierarchical'
      left join core_learningunit lu2 on lum2.secondary_learning_unit_id = lu2.id and lu2.status in ('published','archived')
      left join core_learningunitmap lum3 on lum3.primary_learning_unit_id = lu2.id and lum3.relation_type = 'hierarchical'
      left join core_learningunit lu3 on lum3.secondary_learning_unit_id = lu3.id and lu3.status in ('published','archived')
          and (
              lu3.learning_unit_type in ('subsection', 'video', 'question')
              or lu3.learning_unit_type = 'host' and exists (select 1 from core_proof p where p.learning_unit_id = lu3.id)
              )
  )

    , aggregates AS
    (
    select
        course_id,
        course_name,
        sum(case when module_learning_unit_type = 'book_module' then 1 else 0 end) as book_all,
        sum(case when module_learning_unit_type = 'video_module' then 1 else 0 end) as video_all,
        sum(case when module_learning_unit_type = 'lab' then 1 else 0 end) as lab_all,
        sum(case when module_learning_unit_type = 'book_module' and learning_unit_type = 'question' then 1 else 0 end) as exercise_all
      from included_lu
      where learning_unit_type is not null
      group by course_id, course_name
    )

  select course_id, course_name, book_all, video_all, lab_all, exercise_all, (coalesce(book_all,0)+coalesce(video_all,0)+coalesce(lab_all,0)+coalesce(exercise_all,0)) as total_all
  from aggregates
)

, progress AS
(
SELECT
  user_id
  , t1.course_id
  , t1.course
  , ROUND(book_completed *100 / total_learningunits.book_all)  AS `%topic_progress`
  , ROUND(video_completed *100 / total_learningunits.video_all)  AS `%video_progress`
  , ROUND(exercise_completed *100 / total_learningunits.exercise_all)  AS `%exercise_progress`
  , ROUND(lab_completed *100 / total_learningunits.lab_all)  AS `%lab_progress`
  , ROUND((book_completed + video_completed + exercise_completed+lab_completed) * 100 / total_learningunits.total_all) AS `%total_progress`

FROM
  (
    SELECT 
    core_progressaggregate.user_id AS user_id
    , root_learningunit.id AS course_id
    , root_learningunit.name AS course
    , COALESCE(SUM(case when core_progressaggregate.learning_path_type  = "book_module" then core_progressaggregate.completed else NULL end ), 0) AS book_completed
    , COALESCE(SUM(case when core_progressaggregate.learning_path_type  = "video_module" then core_progressaggregate.completed else NULL end ), 0) AS video_completed
    , COALESCE(SUM(case when core_progressaggregate.learning_path_type  = "book_module" then core_progressaggregate.correct_answers else NULL end ), 0) AS exercise_completed
    , COALESCE(SUM(case when core_progressaggregate.learning_path_type = "lab" then core_progressaggregate.completed else NULL end ), 0) AS lab_completed
    FROM offsec_platform.core_progressaggregate  AS core_progressaggregate
    LEFT JOIN offsec_platform.core_learningunit  AS lu_section ON core_progressaggregate.learning_path_id = lu_section.id
    LEFT JOIN offsec_platform.core_learningunitmap  AS lum_section ON lum_section.secondary_learning_unit_id = lu_section.id and lum_section.relation_type = 'hierarchical'
    LEFT JOIN offsec_platform.core_learningunit  AS root_learningunit ON lum_section.top_level_learning_unit_id =root_learningunit.id
    GROUP BY 1,2,3
  ) AS t1
LEFT JOIN total_learningunits ON t1.course_id = total_learningunits.course_id
-- GROUP BY 1,2,3
)

, downloads AS 
(
  SELECT
    `core_userdownload`.`user_id` AS user_id
    , `core_learningunit`.`name` AS course
  FROM `offsec_platform`.`core_userdownload` AS `core_userdownload`
  LEFT JOIN `offsec_platform`.`core_courseversion` AS `core_courseversion` ON `core_userdownload`.`course_version_id` = `core_courseversion`.`id`
  LEFT JOIN `offsec_platform`.`core_learningunit` AS `core_learningunit` ON `core_courseversion`.`learning_unit_id` = `core_learningunit`.`id`
  WHERE `core_userdownload`.`downloaded_at` IS NOT NULL
  GROUP BY 1,2
)

, all_exams AS
(
  SELECT
    `v_users`.`id` AS user_id
    , usercontentaccess_ptr_id
    , core_orderproduct.id AS orderproduct_id
    , (DATE(`core_examattempt`.`starts_at`)) AS starts_date
    , CASE WHEN core_examattempt.exam_type = "awae" THEN "WEB-300"
              WHEN core_examattempt.exam_type = "ctp" THEN "CTP"
              WHEN core_examattempt.exam_type = "etbd"  THEN "PEN-300"
              WHEN core_examattempt.exam_type = "exp312"  THEN "EXP-312"
              WHEN core_examattempt.exam_type = "klr" THEN "KLCP"
              WHEN core_examattempt.exam_type = "pwk" THEN "PEN-200"
              WHEN core_examattempt.exam_type = "soc200"  THEN "SOC-200"
              WHEN core_examattempt.exam_type = "web200"  THEN "WEB-200"
              WHEN core_examattempt.exam_type = "wifu"  THEN "PEN-210"
              WHEN core_examattempt.exam_type = "wumed" THEN "EXP-301"
          ELSE core_examattempt.exam_type END AS course
    , `core_examattempt`.`status` AS status
    , `examattempt_status`.`examattempt_status_name` AS status_name
  FROM `offsec_platform`.`core_examattempt` AS `core_examattempt`
  LEFT JOIN `analytics`.`examattempt_status` AS `examattempt_status` ON `core_examattempt`.`status` = `examattempt_status`.`id`
  LEFT JOIN `offsec_platform`.`core_usercontentaccessthroughorders` AS `core_usercontentaccessthroughorders` ON `core_examattempt`.`usercontentaccess_ptr_id` = `core_usercontentaccessthroughorders`.`user_content_access_id`
  LEFT JOIN `offsec_platform`.`core_orderproduct` AS `core_orderproduct` ON `core_usercontentaccessthroughorders`.`order_product_id` = `core_orderproduct`.`id`
  LEFT JOIN `offsec_platform`.`core_order` AS `core_order` ON `core_orderproduct`.`order_id` = `core_order`.`id`
  LEFT JOIN `offsec_platform`.`v_users` AS `v_users` ON `core_order`.`user_id` = `v_users`.`id`
  GROUP BY 1,2,3,4,5,6,7
)

, exam_attempts AS
(
  SELECT
    user_id
    , course
    , orderproduct_id
    , COUNT(DISTINCT case when status in (13, 14, 16, 1, 9) then  usercontentaccess_ptr_id  END ) AS count_attempts
  FROM all_exams
  GROUP BY 1,2,3
)

, exam_scheduled AS
(
  SELECT
      user_id
      , course
      , orderproduct_id
      , starts_date AS scheduled_date
  FROM all_exams
  WHERE status_name = 'Scheduled'
  GROUP BY 1,2,3,4
)

, latest_exams AS
(
  SELECT
    user_id
    , course
    , orderproduct_id
    , MAX(starts_date) AS last_exam_attempt_date
    , SUBSTRING_INDEX(GROUP_CONCAT(status_name ORDER BY starts_date DESC),',',1)   AS  last_exam_result
  FROM all_exams
  WHERE starts_date IS NOT NULL and status_name <> 'Scheduled'
  GROUP BY 1,2,3
)

SELECT 
a.os_id, a.last_login_date, a.email, a.student_type, a.company_name, a.master_account_osid, a.country, a.order_id, a.order_due_date, a.payment_time
, CASE WHEN student_type = 'b2c' THEN g.price ELSE h.price END AS price
, a.access_start_date, a.access_end_date, a.purchase_type, a.course, a.order_type, a.order_status
, CASE WHEN c.course IS NOT NULL THEN 'y' ELSE 'n' END AS downloaded_material
, b.`%topic_progress`
, b.`%video_progress`
, b.`%exercise_progress`
, b.`%lab_progress`
, b.`%total_progress`
, d.count_attempts AS `#exam_attempts`
, e.scheduled_date AS next_exam_date
, f.last_exam_attempt_date
, f.last_exam_result
FROM allocated AS a
LEFT JOIN progress AS b ON a.course = b.course AND a.user_id = b.user_id AND access_start_date IS NOT NULL
LEFT JOIN downloads AS c ON a.course = c.course AND a.user_id = c.user_id AND access_start_date IS NOT NULL
LEFT JOIN exam_attempts AS d ON a.course = d.course AND a.user_id = d.user_id AND a.orderproduct_id = d.orderproduct_id
LEFT JOIN exam_scheduled AS e ON a.course = e.course AND a.user_id = e.user_id AND a.orderproduct_id = e.orderproduct_id
LEFT JOIN latest_exams AS f ON a.course = f.course AND a.user_id = f.user_id AND a.orderproduct_id = f.orderproduct_id
LEFT JOIN order_price AS g USING (order_id)
LEFT JOIN invitation_price AS h USING (invite_id)

