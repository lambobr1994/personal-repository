WITH core_latest_payment_by_order_product_id_pdt AS
(
  SELECT 
    order_product_id 
    , MAX(CASE WHEN core_payment.payment_status = 1 THEN core_payment.id END) AS latest_payment_id
  FROM  offsec_platform.core_orderpayment  AS core_orderpayment
  LEFT JOIN offsec_platform.core_payment  AS core_payment ON core_orderpayment.payment_id  = core_payment.id
  GROUP BY 1
)

, users_with_access AS
(
  SELECT
      v_users.os_id  AS `v_users.os_id`
      , v_users.id  AS user_id
      , v_users.first_name  AS `v_users.first_name`
      , v_users.last_name  AS `v_users.last_name`
      , v_users.email  AS `v_users.email`
      , DATE(core_usercontentaccess.start ) AS `core_usercontentaccess.start_date`
      , DATE(core_usercontentaccess.end ) AS `core_usercontentaccess.end_date`
      , core_learningunit.name  AS `core_learningunit.name`
      , L1_courseselection_product.name
      , core_orderproduct.id AS orderproduct_id
      , CASE WHEN accountordersusers.invite_id IS NOT NULL THEN 'B2B' ELSE 'B2C' END AS 'B2B/B2C'
      , accounts.account_name
      , core_payment.payment_method_type
      , payment_provider.payment_provider_name
      
  FROM offsec_platform.core_usercontentaccess  AS core_usercontentaccess
  LEFT JOIN offsec_platform.core_learningunit  AS core_learningunit ON core_usercontentaccess.learning_unit_id =core_learningunit.id
  LEFT JOIN offsec_platform.core_subscriptioncourseselection  AS core_subscriptioncourseselection ON core_usercontentaccess.id = core_subscriptioncourseselection.user_content_access_id
  LEFT JOIN offsec_platform.core_product  AS L1_courseselection_product ON core_subscriptioncourseselection.product_id = L1_courseselection_product.id
  LEFT JOIN offsec_platform.v_users  AS v_users ON core_usercontentaccess.user_id =v_users.id
  
  LEFT JOIN core_usercontentaccessthroughorders ON core_usercontentaccess.id = core_usercontentaccessthroughorders.user_content_access_id
  LEFT JOIN core_orderproduct ON core_usercontentaccessthroughorders.order_product_id = core_orderproduct.id
  LEFT JOIN accountordersusers ON core_orderproduct.order_id = accountordersusers.order_id
  LEFT JOIN accounts ON accountordersusers.account_id = accounts.id
  
  LEFT JOIN core_latest_payment_by_order_product_id_pdt ON core_orderproduct.id = core_latest_payment_by_order_product_id_pdt.order_product_id
  LEFT JOIN core_payment ON core_latest_payment_by_order_product_id_pdt.latest_payment_id  = core_payment.id
  LEFT JOIN analytics.payment_provider ON core_payment.payment_provider = payment_provider.id
  
  WHERE (core_usercontentaccess.end ) >= ((DATE_ADD(DATE(NOW()),INTERVAL 1 day))) 
        AND 
           ( (core_learningunit.name IN ('Learn Unlimited', 'SOC-200') AND  L1_courseselection_product.name  IS null)
              OR (core_learningunit.name IN ('Learn One') AND  L1_courseselection_product.name  = 'SOC-200') )
        AND (NOT (case when v_users.email like '%evozon%' or
              v_users.email like '%offensive-security.com%' or
              v_users.email like '%accelone%'
              then True else False end  ) OR (case when v_users.email like '%evozon%' or
              v_users.email like '%offensive-security.com%' or
              v_users.email like '%accelone%'
              then True else False end  ) IS NULL)
)

, last_SOC200_activity AS
(
  SELECT
      `v_users`.`os_id` AS `v_users.os_id`
      , DATE(MAX(DATE(core_activitylog.timestamp ))) AS last_activity_date
  FROM
      `analytics`.`core_activitylog` AS `core_activitylog`
      LEFT JOIN `offsec_platform`.`v_users` AS `v_users` ON `core_activitylog`.`user_id` = `v_users`.`id`
      LEFT JOIN `offsec_platform`.`core_learningunit` AS `root_learningunit` ON `core_activitylog`.`root_learning_path_id` = `root_learningunit`.`id`
  WHERE `root_learningunit`.`name` = 'SOC-200'
  GROUP BY 1
)

,  total_learningunits AS (
    with included_lu as
    (
      select lu_top.id AS course_id, lu_top.name as course_name, lu_top.code as course_code, lu_module.id as module_id, lu_module.learning_unit_type as module_learning_unit_type,
          lu1.id, lu1.name, lu1.learning_unit_type
      from core_learningunit lu_top
      join core_learningunitmap lum0 on lum0.top_level_learning_unit_id = lu_top.id and lum0.relation_type = 'hierarchical'
      join core_learningunit lu_module on lum0.secondary_learning_unit_id = lu_module.id and lu_module.learning_unit_type in ('book_module', 'video_module', 'lab')
      left join core_learningunitmap lum1 on lum1.primary_learning_unit_id = lu_module.id and lum1.relation_type = 'hierarchical'
      left join core_learningunit lu1 on lum1.secondary_learning_unit_id = lu1.id and lu1.status in ('published','archived')
          and (
              lu1.learning_unit_type in ('subsection', 'video', 'question')
              or lu1.learning_unit_type = 'host' and exists (select 1 from core_proof p where p.learning_unit_id = lu1.id)
              )
      where lu_top.name = 'SOC-200'

      union

      select lu_top.id AS course_id, lu_top.name as course_name, lu_top.code as course_code, lu_module.id as module_id, lu_module.learning_unit_type as module_learning_unit_type,
          lu2.id, lu2.name, lu2.learning_unit_type
      from core_learningunit lu_top
      join core_learningunitmap lum0 on lum0.top_level_learning_unit_id = lu_top.id and lum0.relation_type = 'hierarchical'
      join core_learningunit lu_module on lum0.secondary_learning_unit_id = lu_module.id and lu_module.learning_unit_type in ('book_module', 'video_module', 'lab')
      left join core_learningunitmap lum1 on lum1.primary_learning_unit_id = lu_module.id and lum1.relation_type = 'hierarchical'
      left join core_learningunit lu1 on lum1.secondary_learning_unit_id = lu1.id and lu1.status in ('published','archived')
      left join core_learningunitmap lum2 on lum2.primary_learning_unit_id = lu1.id and lum2.relation_type = 'hierarchical'
      left join core_learningunit lu2 on lum2.secondary_learning_unit_id = lu2.id and lu2.status in ('published','archived')
          and (
              lu2.learning_unit_type in ('subsection', 'video', 'question')
              or lu2.learning_unit_type = 'host' and exists (select 1 from core_proof p where p.learning_unit_id = lu2.id)
              )
      where lu_top.name = 'SOC-200'

      union

      select lu_top.id AS course_id, lu_top.name as course_name, lu_top.code as course_code, lu_module.id as module_id, lu_module.learning_unit_type as module_learning_unit_type,
          lu3.id, lu3.name, lu3.learning_unit_type
      from core_learningunit lu_top
      join core_learningunitmap lum0 on lum0.top_level_learning_unit_id = lu_top.id and lum0.relation_type = 'hierarchical'
      join core_learningunit lu_module on lum0.secondary_learning_unit_id = lu_module.id and lu_module.learning_unit_type in ('book_module', 'video_module', 'lab')
      left join core_learningunitmap lum1 on lum1.primary_learning_unit_id = lu_module.id and lum1.relation_type = 'hierarchical'
      left join core_learningunit lu1 on lum1.secondary_learning_unit_id = lu1.id and lu1.status in ('published','archived')
      left join core_learningunitmap lum2 on lum2.primary_learning_unit_id = lu1.id and lum2.relation_type = 'hierarchical'
      left join core_learningunit lu2 on lum2.secondary_learning_unit_id = lu2.id and lu2.status in ('published','archived')
      left join core_learningunitmap lum3 on lum3.primary_learning_unit_id = lu2.id and lum3.relation_type = 'hierarchical'
      left join core_learningunit lu3 on lum3.secondary_learning_unit_id = lu3.id and lu3.status in ('published','archived')
          and (
              lu3.learning_unit_type in ('subsection', 'video', 'question')
              or lu3.learning_unit_type = 'host' and exists (select 1 from core_proof p where p.learning_unit_id = lu3.id)
              )
      where lu_top.name = 'SOC-200'
  )

    , aggregates AS
    (
    select
        course_id,
        course_name,
        sum(case when module_learning_unit_type = 'book_module' then 1 else 0 end) as book_all,
        sum(case when module_learning_unit_type = 'video_module' then 1 else 0 end) as video_all,
        sum(case when module_learning_unit_type = 'lab' then 1 else 0 end) as lab_all,
        sum(case when module_learning_unit_type = 'book_module' and learning_unit_type = 'question' then 1 else 0 end) as exercise_all
      from included_lu
      where learning_unit_type is not null
      group by course_id, course_name
    )
  select course_id, course_name, book_all, video_all, lab_all, exercise_all, (coalesce(book_all,0)+coalesce(video_all,0)+coalesce(lab_all,0)+coalesce(exercise_all,0)) as total_all
  from aggregates
)

-- , progress AS
-- (
--   SELECT 
--   core_progressaggregate.user_id AS user_id
--   , root_learningunit.name AS course
--   , ROUND(COALESCE(SUM(case when core_progressaggregate.learning_path_type  = "book_module" then core_progressaggregate.completed else NULL end ), 0) * 100 / total_learningunits.book_all)  AS `%book_progress`
--     , ROUND(COALESCE(SUM(case when core_progressaggregate.learning_path_type  = "video_module" then core_progressaggregate.completed else NULL end ), 0) * 100 / total_learningunits.video_all)  AS `%video_progress`
--     , ROUND(COALESCE(SUM(case when core_progressaggregate.learning_path_type  = "book_module" then core_progressaggregate.correct_answers else NULL end ), 0) * 100 / total_learningunits.exercise_all)  AS `%exercise_progress`
--     , ROUND(COALESCE(SUM(core_progressaggregate.completed ), 0) * 100/ total_learningunits.total_all)  AS `%total_progress`
--   FROM offsec_platform.core_progressaggregate  AS core_progressaggregate
--   LEFT JOIN offsec_platform.core_learningunit  AS lu_section ON core_progressaggregate.learning_path_id = lu_section.id
--   LEFT JOIN offsec_platform.core_learningunitmap  AS lum_section ON lum_section.secondary_learning_unit_id = lu_section.id and lum_section.relation_type = 'hierarchical'
--   LEFT JOIN offsec_platform.core_learningunit  AS root_learningunit ON lum_section.top_level_learning_unit_id =root_learningunit.id
--   LEFT JOIN total_learningunits ON root_learningunit.id = total_learningunits.course_id
--   WHERE root_learningunit.name = 'SOC-200'
--   GROUP BY 1,2
-- )

, progress AS
(
SELECT
  user_id
  , t1.course_id
  , ROUND(book_completed *100 / total_learningunits.book_all)  AS `%book_progress`
  , ROUND(video_completed *100 / total_learningunits.video_all)  AS `%video_progress`
  , ROUND(exercise_completed *100 / total_learningunits.exercise_all)  AS `%exercise_progress`
  , ROUND((book_completed + video_completed + exercise_completed) * 100 / total_learningunits.total_all) AS `%total_progress`

FROM
  (
    SELECT 
    core_progressaggregate.user_id AS user_id
    , root_learningunit.id AS course_id
    , COALESCE(SUM(case when core_progressaggregate.learning_path_type  = "book_module" then core_progressaggregate.completed else NULL end ), 0) AS book_completed
    , COALESCE(SUM(case when core_progressaggregate.learning_path_type  = "video_module" then core_progressaggregate.completed else NULL end ), 0) AS video_completed
    , COALESCE(SUM(case when core_progressaggregate.learning_path_type  = "book_module" then core_progressaggregate.correct_answers else NULL end ), 0) AS exercise_completed
    FROM offsec_platform.core_progressaggregate  AS core_progressaggregate
    LEFT JOIN offsec_platform.core_learningunit  AS lu_section ON core_progressaggregate.learning_path_id = lu_section.id
    LEFT JOIN offsec_platform.core_learningunitmap  AS lum_section ON lum_section.secondary_learning_unit_id = lu_section.id and lum_section.relation_type = 'hierarchical'
    LEFT JOIN offsec_platform.core_learningunit  AS root_learningunit ON lum_section.top_level_learning_unit_id =root_learningunit.id
    WHERE root_learningunit.name = 'SOC-200'
    GROUP BY 1, 2
  ) AS t1
LEFT JOIN total_learningunits ON t1.course_id = total_learningunits.course_id
-- GROUP BY 1,2
)


SELECT a.*, b.last_activity_date, c.`%book_progress`, c.`%video_progress`, c.`%exercise_progress`, c.`%total_progress`
FROM users_with_access AS a
LEFT JOIN last_SOC200_activity AS b ON a.`v_users.os_id` = b.`v_users.os_id`
LEFT JOIN progress AS c ON a.user_id = c.user_id
