WITH users_with_access AS
(
  SELECT
      v_users.os_id  AS os_id
      , v_users.id  AS user_id
      , v_users.first_name as first_name
      , v_users.last_name as last_name
      , v_users.email as email
      , v_users.country_name  AS country
      , DATE(v_users.last_login) AS last_login_date
      , accounts.account_name AS company_name
      , admin.os_id AS master_account_OSID
      , DATE(core_usercontentaccess.start ) AS access_start_date
      , DATE(core_usercontentaccess.end ) AS access_end_date
      , CASE WHEN accountordersusers.invite_id IS NOT NULL THEN 'B2B' ELSE 'B2C' END AS student_type
      , CASE WHEN core_learningunit.name = 'Learn One' THEN 'L1'
             WHEN core_learningunit.name = 'Learn Unlimited' THEN 'LU' 
             WHEN core_learningunit.name = 'Learn Fundamentals' THEN 'LF' 
             ELSE 'Standalone' 
        END AS purchase_type
      , CASE WHEN core_learningunit.name = 'Learn Unlimited' THEN 'LU'
             WHEN core_learningunit.name = 'Learn Fundamentals' THEN 'LF'
             ELSE coalesce(L1_courseselection_product.name, core_learningunit.name)
        END AS course
      , core_orderproduct.id AS orderproduct_id
      , order_type.order_type_name  AS order_type
      , order_status.order_status_name AS order_status
      , core_order.order_due_date AS order_date
  FROM offsec_platform.core_usercontentaccess  AS core_usercontentaccess
  LEFT JOIN offsec_platform.core_learningunit  AS core_learningunit ON core_usercontentaccess.learning_unit_id =core_learningunit.id
  LEFT JOIN offsec_platform.core_subscriptioncourseselection  AS core_subscriptioncourseselection ON core_usercontentaccess.id = core_subscriptioncourseselection.user_content_access_id
  LEFT JOIN offsec_platform.core_product  AS L1_courseselection_product ON core_subscriptioncourseselection.product_id = L1_courseselection_product.id
  LEFT JOIN offsec_platform.v_users  AS v_users ON core_usercontentaccess.user_id =v_users.id
  LEFT JOIN core_usercontentaccessthroughorders ON core_usercontentaccess.id = core_usercontentaccessthroughorders.user_content_access_id
  LEFT JOIN core_orderproduct ON core_usercontentaccessthroughorders.order_product_id = core_orderproduct.id
  LEFT JOIN core_order ON core_orderproduct.order_id = core_order.id
  LEFT JOIN analytics.order_status ON core_order.order_status = order_status.id
  LEFT JOIN analytics.order_type  AS order_type ON core_order.order_type  =order_type.id
  LEFT JOIN accountordersusers ON core_orderproduct.order_id = accountordersusers.order_id
  LEFT JOIN accounts ON accountordersusers.account_id = accounts.id
  LEFT JOIN invitations ON accountordersusers.invite_id = invitations.id
  LEFT JOIN offsec_platform.v_users AS admin ON invitations.created_by = admin.id
  WHERE 1=1
        -- AND (core_usercontentaccess.end ) >= ((DATE_ADD(DATE(NOW()),INTERVAL 1 day))) 
        -- AND (core_usercontentaccess.start ) >= '2022-01-01'     -- filter date
        AND core_learningunit.name = 'Learn Unlimited'          -- filter what product type
        -- AND accountordersusers.invite_id IS NOT NULL            -- remove if want to include b2c
        AND v_users.email NOT LIKE '%evozon%' 
        AND v_users.email NOT LIKE '%offensive-security.com%'
        AND v_users.email NOT LIKE '%accelone%'
        AND core_learningunit.name NOT IN ('exam','lab','practice','play','pwk live','n/a')
        AND invitations.cancelled_at IS NULL
  GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18
)

, sub_allocation AS
(
  -- manual allocation of LF and LU courses
  SELECT 'LF' AS sub, 'PEN-100' AS course UNION ALL
  SELECT 'LF', 'WEB-100' UNION ALL
  SELECT 'LF', 'SOC-100' UNION ALL
  SELECT 'LF', 'CLD-100' UNION ALL
  SELECT 'LF', 'EXP-100' UNION ALL
  SELECT 'LF', 'PEN-103' UNION ALL
  SELECT 'LF', 'PEN-210' UNION ALL 
  SELECT 'LU', 'PEN-100' UNION ALL
  SELECT 'LU', 'WEB-100' UNION ALL
  SELECT 'LU', 'SOC-100' UNION ALL
  SELECT 'LU', 'CLD-100' UNION ALL
  SELECT 'LU', 'EXP-100' UNION ALL
  SELECT 'LU', 'PEN-103' UNION ALL
  SELECT 'LU', 'PEN-210' UNION ALL 
  SELECT 'LU', 'PEN-200' UNION ALL 
  SELECT 'LU', 'WEB-200' UNION ALL 
  SELECT 'LU', 'SOC-200' UNION ALL 
  SELECT 'LU', 'PEN-300' UNION ALL 
  SELECT 'LU', 'WEB-300' UNION ALL 
  SELECT 'LU', 'EXP-301' UNION ALL 
  SELECT 'LU', 'EXP-312'
)

, allocated AS 
(
SELECT a.os_id, a.user_id, a.first_name, a.last_name, a.email, a.last_login_date, a.company_name, a.master_account_osid, a.country, a.access_start_date, a.access_end_date, a.student_type, a.purchase_type
  , case when a.course IN ('LF','LU') THEN b.course ELSE a.course END AS course
  , a.orderproduct_id , a.order_type, a.order_status, a.order_date
FROM users_with_access AS a
LEFT JOIN sub_allocation AS b ON a.course = b.sub
)

, total_learningunits AS (
    with included_lu as
    (
      select lu_top.id AS course_id, lu_top.name as course_name, lu_top.code as course_code, lu_module.id as module_id, lu_module.learning_unit_type as module_learning_unit_type,
          lu1.id, lu1.name, lu1.learning_unit_type
      from core_learningunit lu_top
      join core_learningunitmap lum0 on lum0.top_level_learning_unit_id = lu_top.id and lum0.relation_type = 'hierarchical'
      join core_learningunit lu_module on lum0.secondary_learning_unit_id = lu_module.id and lu_module.learning_unit_type in ('book_module', 'video_module', 'lab')
      left join core_learningunitmap lum1 on lum1.primary_learning_unit_id = lu_module.id and lum1.relation_type = 'hierarchical'
      left join core_learningunit lu1 on lum1.secondary_learning_unit_id = lu1.id and lu1.status in ('published','archived')
          and (
              lu1.learning_unit_type in ('subsection', 'video', 'question')
              or lu1.learning_unit_type = 'host' and exists (select 1 from core_proof p where p.learning_unit_id = lu1.id)
              )

      union

      select lu_top.id AS course_id, lu_top.name as course_name, lu_top.code as course_code, lu_module.id as module_id, lu_module.learning_unit_type as module_learning_unit_type,
          lu2.id, lu2.name, lu2.learning_unit_type
      from core_learningunit lu_top
      join core_learningunitmap lum0 on lum0.top_level_learning_unit_id = lu_top.id and lum0.relation_type = 'hierarchical'
      join core_learningunit lu_module on lum0.secondary_learning_unit_id = lu_module.id and lu_module.learning_unit_type in ('book_module', 'video_module', 'lab')
      left join core_learningunitmap lum1 on lum1.primary_learning_unit_id = lu_module.id and lum1.relation_type = 'hierarchical'
      left join core_learningunit lu1 on lum1.secondary_learning_unit_id = lu1.id and lu1.status in ('published','archived')
      left join core_learningunitmap lum2 on lum2.primary_learning_unit_id = lu1.id and lum2.relation_type = 'hierarchical'
      left join core_learningunit lu2 on lum2.secondary_learning_unit_id = lu2.id and lu2.status in ('published','archived')
          and (
              lu2.learning_unit_type in ('subsection', 'video', 'question')
              or lu2.learning_unit_type = 'host' and exists (select 1 from core_proof p where p.learning_unit_id = lu2.id)
              )

      union

      select lu_top.id AS course_id, lu_top.name as course_name, lu_top.code as course_code, lu_module.id as module_id, lu_module.learning_unit_type as module_learning_unit_type,
          lu3.id, lu3.name, lu3.learning_unit_type
      from core_learningunit lu_top
      join core_learningunitmap lum0 on lum0.top_level_learning_unit_id = lu_top.id and lum0.relation_type = 'hierarchical'
      join core_learningunit lu_module on lum0.secondary_learning_unit_id = lu_module.id and lu_module.learning_unit_type in ('book_module', 'video_module', 'lab')
      left join core_learningunitmap lum1 on lum1.primary_learning_unit_id = lu_module.id and lum1.relation_type = 'hierarchical'
      left join core_learningunit lu1 on lum1.secondary_learning_unit_id = lu1.id and lu1.status in ('published','archived')
      left join core_learningunitmap lum2 on lum2.primary_learning_unit_id = lu1.id and lum2.relation_type = 'hierarchical'
      left join core_learningunit lu2 on lum2.secondary_learning_unit_id = lu2.id and lu2.status in ('published','archived')
      left join core_learningunitmap lum3 on lum3.primary_learning_unit_id = lu2.id and lum3.relation_type = 'hierarchical'
      left join core_learningunit lu3 on lum3.secondary_learning_unit_id = lu3.id and lu3.status in ('published','archived')
          and (
              lu3.learning_unit_type in ('subsection', 'video', 'question')
              or lu3.learning_unit_type = 'host' and exists (select 1 from core_proof p where p.learning_unit_id = lu3.id)
              )
  )

    , aggregates AS
    (
    select
        course_id,
        course_name,
        sum(case when module_learning_unit_type = 'book_module' then 1 else 0 end) as book_all,
        sum(case when module_learning_unit_type = 'video_module' then 1 else 0 end) as video_all,
        sum(case when module_learning_unit_type = 'lab' then 1 else 0 end) as lab_all,
        sum(case when module_learning_unit_type = 'book_module' and learning_unit_type = 'question' then 1 else 0 end) as exercise_all
      from included_lu
      where learning_unit_type is not null
      group by course_id, course_name
    )

  select course_id, course_name, book_all, video_all, lab_all, exercise_all, (coalesce(book_all,0)+coalesce(video_all,0)+coalesce(lab_all,0)+coalesce(exercise_all,0)) as total_all
  from aggregates
)

, progress AS
(
SELECT
  user_id
  , t1.course_id
  , t1.course
  , lab_completed AS count_lab_completed
  , ROUND(book_completed *100 / total_learningunits.book_all)  AS `%topic_progress`
  , ROUND(video_completed *100 / total_learningunits.video_all)  AS `%video_progress`
  , ROUND(exercise_completed *100 / total_learningunits.exercise_all)  AS `%exercise_progress`
  , ROUND(lab_completed *100 / total_learningunits.lab_all)  AS `%lab_progress`
  , ROUND((book_completed + video_completed + exercise_completed+lab_completed) * 100 / total_learningunits.total_all) AS `%total_progress`

FROM
  (
    SELECT 
    core_progressaggregate.user_id AS user_id
    , root_learningunit.id AS course_id
    , root_learningunit.name AS course
    , COALESCE(SUM(case when core_progressaggregate.learning_path_type  = "book_module" then core_progressaggregate.completed else NULL end ), 0) AS book_completed
    , COALESCE(SUM(case when core_progressaggregate.learning_path_type  = "video_module" then core_progressaggregate.completed else NULL end ), 0) AS video_completed
    , COALESCE(SUM(case when core_progressaggregate.learning_path_type  = "book_module" then core_progressaggregate.correct_answers else NULL end ), 0) AS exercise_completed
    , COALESCE(SUM(case when core_progressaggregate.learning_path_type = "lab" then core_progressaggregate.completed else NULL end ), 0) AS lab_completed
    FROM offsec_platform.core_progressaggregate  AS core_progressaggregate
    LEFT JOIN offsec_platform.core_learningunit  AS lu_section ON core_progressaggregate.learning_path_id = lu_section.id
    LEFT JOIN offsec_platform.core_learningunitmap  AS lum_section ON lum_section.secondary_learning_unit_id = lu_section.id and lum_section.relation_type = 'hierarchical'
    LEFT JOIN offsec_platform.core_learningunit  AS root_learningunit ON lum_section.top_level_learning_unit_id =root_learningunit.id
    GROUP BY 1,2,3
  ) AS t1
LEFT JOIN total_learningunits ON t1.course_id = total_learningunits.course_id
-- GROUP BY 1,2,3
)

, downloads AS 
(
  SELECT
    `core_userdownload`.`user_id` AS user_id
    , `core_learningunit`.`name` AS course
  FROM `offsec_platform`.`core_userdownload` AS `core_userdownload`
  LEFT JOIN `offsec_platform`.`core_courseversion` AS `core_courseversion` ON `core_userdownload`.`course_version_id` = `core_courseversion`.`id`
  LEFT JOIN `offsec_platform`.`core_learningunit` AS `core_learningunit` ON `core_courseversion`.`learning_unit_id` = `core_learningunit`.`id`
  WHERE `core_userdownload`.`downloaded_at` IS NOT NULL
  GROUP BY 1,2
)

, all_exams AS
(
  SELECT
    `v_users`.`id` AS user_id
    , usercontentaccess_ptr_id
    , core_orderproduct.id AS orderproduct_id
    , (DATE(`core_examattempt`.`starts_at`)) AS starts_date
    , CASE WHEN core_examattempt.exam_type = "awae" THEN "WEB-300"
              WHEN core_examattempt.exam_type = "ctp" THEN "CTP"
              WHEN core_examattempt.exam_type = "etbd"  THEN "PEN-300"
              WHEN core_examattempt.exam_type = "exp312"  THEN "EXP-312"
              WHEN core_examattempt.exam_type = "klr" THEN "KLCP"
              WHEN core_examattempt.exam_type = "pwk" THEN "PEN-200"
              WHEN core_examattempt.exam_type = "soc200"  THEN "SOC-200"
              WHEN core_examattempt.exam_type = "web200"  THEN "WEB-200"
              WHEN core_examattempt.exam_type = "wifu"  THEN "PEN-210"
              WHEN core_examattempt.exam_type = "wumed" THEN "EXP-301"
          ELSE core_examattempt.exam_type END AS course
    , `core_examattempt`.`status` AS status
    , `examattempt_status`.`examattempt_status_name` AS status_name
  FROM `offsec_platform`.`core_examattempt` AS `core_examattempt`
  LEFT JOIN `analytics`.`examattempt_status` AS `examattempt_status` ON `core_examattempt`.`status` = `examattempt_status`.`id`
  LEFT JOIN `offsec_platform`.`core_usercontentaccessthroughorders` AS `core_usercontentaccessthroughorders` ON `core_examattempt`.`usercontentaccess_ptr_id` = `core_usercontentaccessthroughorders`.`user_content_access_id`
  LEFT JOIN `offsec_platform`.`core_orderproduct` AS `core_orderproduct` ON `core_usercontentaccessthroughorders`.`order_product_id` = `core_orderproduct`.`id`
  LEFT JOIN `offsec_platform`.`core_order` AS `core_order` ON `core_orderproduct`.`order_id` = `core_order`.`id`
  LEFT JOIN `offsec_platform`.`v_users` AS `v_users` ON `core_order`.`user_id` = `v_users`.`id`
  GROUP BY 1,2,3,4,5,6,7
)

, exam_attempts AS
(
  SELECT
    user_id
    , course
    , orderproduct_id
    , COUNT(DISTINCT case when status in (13, 14, 16, 1, 9) then  usercontentaccess_ptr_id  END ) AS count_attempts
  FROM all_exams
  GROUP BY 1,2,3
)

, exam_scheduled AS
(
  SELECT
      user_id
      , course
      , orderproduct_id
      , starts_date AS scheduled_date
  FROM all_exams
  WHERE status_name = 'Scheduled'
  GROUP BY 1,2,3,4
)

, latest_exams AS
(
  SELECT
    user_id
    , course
    , orderproduct_id
    , MAX(starts_date) AS last_exam_attempt_date
    , SUBSTRING_INDEX(GROUP_CONCAT(status_name ORDER BY starts_date DESC),',',1)   AS  last_exam_result
  FROM all_exams
  WHERE starts_date IS NOT NULL and status_name <> 'Scheduled'
  GROUP BY 1,2,3
)

SELECT 
a.os_id, a.first_name, a.last_name, a.email, a.last_login_date, a.company_name, a.master_account_osid, a.country
, a.access_start_date, a.access_end_date, a.student_type, a.purchase_type, a.course, a.order_type, a.order_status, a.order_date
, CASE WHEN c.course IS NOT NULL THEN 'y' ELSE 'n' END AS downloaded_material
, b.`%topic_progress`
, b.`%video_progress`
, b.`%exercise_progress`
, b.`%lab_progress`
, b.`%total_progress`
, b.count_lab_completed
, d.count_attempts AS `#exam_attempts`
, e.scheduled_date AS next_exam_date
, f.last_exam_attempt_date
, f.last_exam_result
FROM allocated AS a
LEFT JOIN progress AS b ON a.course = b.course AND a.user_id = b.user_id
LEFT JOIN downloads AS c ON a.course = c.course AND a.user_id = c.user_id
LEFT JOIN exam_attempts AS d ON a.course = d.course AND a.user_id = d.user_id AND a.orderproduct_id = d.orderproduct_id
LEFT JOIN exam_scheduled AS e ON a.course = e.course AND a.user_id = e.user_id AND a.orderproduct_id = e.orderproduct_id
LEFT JOIN latest_exams AS f ON a.course = f.course AND a.user_id = f.user_id AND a.orderproduct_id = f.orderproduct_id