WITH data AS
(
  SELECT
      `v_users`.`id` AS `v_users.id`
       , (DATE(`core_examattempt`.`starts_at`)) AS `core_examattempt.starts_date`
       , `examattempt_status`.`examattempt_status_name` AS `examattempt_status.examattempt_status_name`
       , COUNT(DISTINCT CASE WHEN  `submission_learningunit`.`learning_unit_type`  = 'host' THEN core_usersubmission.id END) AS `core_usersubmission.count_lab_id`
  FROM
      `offsec_platform`.`core_examattempt` AS `core_examattempt`
      LEFT JOIN `analytics`.`examattempt_status` AS `examattempt_status` ON `core_examattempt`.`status` = `examattempt_status`.`id`
      LEFT JOIN `offsec_platform`.`core_usercontentaccessthroughorders` AS `core_usercontentaccessthroughorders` ON `core_examattempt`.`usercontentaccess_ptr_id` = `core_usercontentaccessthroughorders`.`user_content_access_id`
      LEFT JOIN `offsec_platform`.`core_orderproduct` AS `core_orderproduct` ON `core_usercontentaccessthroughorders`.`order_product_id` = `core_orderproduct`.`id`
      LEFT JOIN `offsec_platform`.`core_order` AS `core_order` ON `core_orderproduct`.`order_id` = `core_order`.`id`
      LEFT JOIN `offsec_platform`.`v_users` AS `v_users` ON `core_order`.`user_id` = `v_users`.`id`
      LEFT JOIN `offsec_platform`.`core_usersubmission` AS `core_usersubmission` ON `v_users`.`id` = `core_usersubmission`.`user_id` AND `core_usersubmission`.`created_at`<=`core_examattempt`.`starts_at`
      LEFT JOIN `offsec_platform`.`core_learningunitmap` AS `submission_learningunitmap` ON `core_usersubmission`.`learning_unit_id` = `submission_learningunitmap`.`secondary_learning_unit_id` AND `submission_learningunitmap`.`relation_type` = 'hierarchical'
      LEFT JOIN `offsec_platform`.`core_learningunit` AS `submission_learningunitmap_top_level` ON `submission_learningunitmap`.`top_level_learning_unit_id` = `submission_learningunitmap_top_level`.`id`
      LEFT JOIN `offsec_platform`.`core_learningunit` AS `submission_learningunit` ON `core_usersubmission`.`learning_unit_id` = `submission_learningunit`.`id`
  WHERE 
      core_examattempt.exam_type = 'pwk' 
      AND `examattempt_status`.`examattempt_status_name` IN ('failed', 'failed_pre_exam', 'passed') 
      AND (NOT (case when v_users.email like '%evozon%' or
              v_users.email like '%offensive-security.com%' or
              v_users.email like '%accelone%'
              then True else False end  ) OR (case when v_users.email like '%evozon%' or
              v_users.email like '%offensive-security.com%' or
              v_users.email like '%accelone%'
              then True else False end  ) IS NULL) 
      AND `submission_learningunitmap_top_level`.`name` = 'PEN-200'
  GROUP BY
      1,2,3
)

SELECT
  CASE
    WHEN `core_usersubmission.count_lab_id` < 11 THEN 'Below 11'
    WHEN `core_usersubmission.count_lab_id`  BETWEEN 11 AND 20 THEN '11 to 20'
    WHEN `core_usersubmission.count_lab_id`  BETWEEN 21 AND 30 THEN '21 to 30'
    WHEN `core_usersubmission.count_lab_id`  BETWEEN 31 AND 40 THEN '31 to 40'
    WHEN `core_usersubmission.count_lab_id`  BETWEEN 41 AND 50 THEN '41 to 50'
    WHEN `core_usersubmission.count_lab_id`  BETWEEN 51 AND 60 THEN '51 to 60'
    WHEN `core_usersubmission.count_lab_id`  BETWEEN 61 AND 70 THEN '61 to 70'
    ELSE '71 or Above'
  END AS tier
  , ROUND(COUNT(CASE WHEN `examattempt_status.examattempt_status_name` = 'passed' THEN 1 END)*100 / COUNT(*)) AS pass_rate
FROM data
WHERE `core_usersubmission.count_lab_id`<71
GROUP BY 1
ORDER BY CASE WHEN tier = 'Below 11' THEN 0 ELSE 1 END ASC, tier ASC
