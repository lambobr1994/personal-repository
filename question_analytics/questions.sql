-- Jeremy's Request

WITH core_usersubmission2 AS
      (
        SELECT 
          user_id
          , learning_unit_id
          , MAX(submission_status) AS max_submission_status
          , COUNT(DISTINCT case when  core_usersubmission.submission_status   = 1 then core_usersubmission.id END ) AS count_correct_submissions
          , COUNT(DISTINCT core_usersubmission.id) AS count_total_submissions
        FROM offsec_platform.core_usersubmission
        WHERE 
          core_usersubmission.created_at >= TIMESTAMP('2022-01-01')
        GROUP BY 1,2
      )
, data AS (
      SELECT
       -- course.name AS `course.name`
       question_module.name  AS `question_module.name`
        , case when length(core_question.question_data)<100 then core_question.question_data
             else concat(LEFT(core_question.question_data,50), " .... ", RIGHT(core_question.question_data,50))  END  AS `core_question.question_data`
        , COUNT(DISTINCT case when core_usersubmission2.max_submission_status = 1 then core_usersubmission2.user_id else NULL end) / COUNT(DISTINCT case when core_usersubmission2.max_submission_status = 2 then core_usersubmission2.user_id else NULL end) AS `correct_to_wrong_students_ratio`
        , SUM(core_usersubmission2.count_correct_submissions)/SUM(core_usersubmission2.count_total_submissions) AS `correct_to_total_sub_ratio`
      FROM offsec_platform.core_learningunit  AS question_module
      INNER JOIN offsec_platform.core_learningunitmap  AS book_map ON question_module.id =book_map.secondary_learning_unit_id and   question_module.learning_unit_type = 'book_module' and  question_module.status = "published" and  book_map.relation_type = "hierarchical"
      INNER JOIN offsec_platform.core_learningunit  AS course ON course.id =book_map.top_level_learning_unit_id
      INNER JOIN offsec_platform.core_learningunitmap  AS section_map ON section_map.primary_learning_unit_id = book_map.secondary_learning_unit_id and section_map.relation_type = "hierarchical"
      INNER JOIN offsec_platform.core_learningunitmap  AS subsection_map ON subsection_map.primary_learning_unit_id = section_map.secondary_learning_unit_id and subsection_map.relation_type = "hierarchical"
      INNER JOIN offsec_platform.core_learningunit  AS subsection ON subsection.id =subsection_map.secondary_learning_unit_id  and subsection.learning_unit_type = 'challenge' and  subsection.status = "published"
      INNER JOIN offsec_platform.core_learningunitmap  AS question_map ON question_map.primary_learning_unit_id = subsection_map.secondary_learning_unit_id and question_map.relation_type = "hierarchical"
      INNER JOIN offsec_platform.core_learningunit  AS question ON question.id =question_map.secondary_learning_unit_id  and question.learning_unit_type = 'question' and  subsection.status = "published"
      LEFT JOIN core_usersubmission2 ON question.id = core_usersubmission2.learning_unit_id
      LEFT JOIN offsec_platform.v_users  AS v_users ON core_usersubmission2.user_id =v_users.id
      LEFT JOIN offsec_platform.core_question  AS core_question ON core_usersubmission2.learning_unit_id =core_question.learning_unit_id
      WHERE (NOT (case when v_users.email like '%evozon%' or
                  v_users.email like '%offensive-security.com%' or
                  v_users.email like '%accelone%'
                  then True else False end  ) OR (case when v_users.email like '%evozon%' or
                  v_users.email like '%offensive-security.com%' or
                  v_users.email like '%accelone%'
                  then True else False end  ) IS NULL) AND course.name IN ('SOC-100', 'PEN-100', 'WEB-100')
      GROUP BY 1,2)
      
SELECT `question_module.name`
    , SUM(CASE WHEN `correct_to_total_sub_ratio`<=0.2 THEN 1 ELSE 0 END) AS count_questions_version1
    , SUM(CASE WHEN `correct_to_wrong_students_ratio`<=0.2 THEN 1 ELSE 0 END) AS count_questions
FROM data
GROUP BY 1
