-- query for getting user and activity count per job role aggregated by week

WITH weekly_activity_count_perLU_pdt AS 
(
  SELECT *, row_number() OVER(ORDER BY user_id, learning_unit_id, core_activitylog_timestamp_week) AS id
  FROM
  (
    SELECT
      user_id,
      `learning_unit_id` AS learning_unit_id,
        (DATE_FORMAT(TIMESTAMP(DATE(DATE_ADD(`timestamp`,INTERVAL (0 - MOD((DAYOFWEEK(`timestamp`) - 1) - 1 + 7, 7)) day))), '%Y-%m-%d')) AS core_activitylog_timestamp_week,
      COUNT(DISTINCT core_activitylog.id ) AS activity_count_dim
    FROM
        `analytics`.`core_activitylog` AS `core_activitylog`
    -- WHERE ((( (DATE_FORMAT(TIMESTAMP(DATE(DATE_ADD(`timestamp`,INTERVAL (0 - MOD((DAYOFWEEK(`timestamp`) - 1) - 1 + 7, 7)) day))), '%Y-%m-%d')) ) >= ((DATE_ADD(TIMESTAMP(DATE(DATE_ADD(DATE(NOW()),INTERVAL (0 - MOD((DAYOFWEEK(DATE(NOW())) - 1) - 1 + 7, 7)) day))),INTERVAL -1 week))) AND ( (DATE_FORMAT(TIMESTAMP(DATE(DATE_ADD(`timestamp`,INTERVAL (0 - MOD((DAYOFWEEK(`timestamp`) - 1) - 1 + 7, 7)) day))), '%Y-%m-%d')) ) < ((DATE_ADD(DATE_ADD(TIMESTAMP(DATE(DATE_ADD(DATE(NOW()),INTERVAL (0 - MOD((DAYOFWEEK(DATE(NOW())) - 1) - 1 + 7, 7)) day))),INTERVAL -1 week),INTERVAL 1 week)))))
    GROUP BY
        1,2,3
    ) t1
)

, data AS 
(
  SELECT DISTINCT user_id, engagement_count_per_job_role.id, core_activitylog_timestamp_week,  core_contenttag.id AS core_contenttag_id, core_contenttag.name AS core_contenttag_name, activity_count_dim
  FROM weekly_activity_count_perLU_pdt AS engagement_count_per_job_role
  INNER JOIN offsec_platform.core_learningunitmap  AS lum_section ON lum_section.secondary_learning_unit_id = engagement_count_per_job_role.learning_unit_id and lum_section.relation_type = 'hierarchical'
  LEFT JOIN offsec_platform.core_learningunit  AS lu_section ON lum_section.primary_learning_unit_id = lu_section.id -- and ( lu_section.learning_unit_type = 'section' OR lu_section.learning_unit_type = 'video_module')
  INNER JOIN offsec_platform.core_learningunitmap  AS lum_topic ON lum_topic.secondary_learning_unit_id = lu_section.id and lum_topic.relation_type = 'hierarchical'
  LEFT JOIN offsec_platform.core_learningunit  AS lu_topic ON lum_topic.primary_learning_unit_id = lu_topic.id -- and ( lu_section.learning_unit_type = 'section' OR lu_section.learning_unit_type = 'video_module')
  LEFT JOIN offsec_platform.core_learningunittag  AS core_learningunittag ON core_learningunittag.learning_unit_id = lu_topic.id
  LEFT JOIN offsec_platform.core_learningunitmap  AS lum_test ON 
      case when core_learningunittag.learning_unit_id is null then lum_topic.primary_learning_unit_id else lum_topic.secondary_learning_unit_id end
      = lum_test.secondary_learning_unit_id
      and
      case when core_learningunittag.learning_unit_id is null then 'navigational' else 'hierarchical' end
      = lum_test.relation_type
  LEFT JOIN offsec_platform.core_learningunittag  AS core_learningunittag_map ON lum_test.primary_learning_unit_id = core_learningunittag_map.learning_unit_id
  LEFT JOIN offsec_platform.core_contenttag  AS core_contenttag ON core_learningunittag_map.content_tag_id = core_contenttag.id
  INNER JOIN offsec_platform.core_contenttagclassification  AS core_contenttagclassification ON core_contenttag.tag_classification_id = core_contenttagclassification.id
  WHERE 
  -- ((( engagement_count_per_job_role.core_activitylog_timestamp_week  ) >= ((DATE_ADD(TIMESTAMP(DATE(DATE_ADD(DATE(NOW()),INTERVAL (0 - MOD((DAYOFWEEK(DATE(NOW())) - 1) - 1 + 7, 7)) day))),INTERVAL -1 week))) AND ( engagement_count_per_job_role.core_activitylog_timestamp_week  ) < ((DATE_ADD(DATE_ADD(TIMESTAMP(DATE(DATE_ADD(DATE(NOW()),INTERVAL (0 - MOD((DAYOFWEEK(DATE(NOW())) - 1) - 1 + 7, 7)) day))),INTERVAL -1 week),INTERVAL 1 week))))) AND 
  (core_contenttagclassification.id ) = 7
)

SELECT 
    core_activitylog_timestamp_week,
    (case when core_contenttag_id in (1254,1255,1260,1271,1267) then "Cybersecurity (Break)"
              when core_contenttag_id in (1256,1269,1268,1270,1257) then "Cybersecurity (Defend)"
              when core_contenttag_id in (1265,1264,1266,1259,1263,1261,1262,1258) then "IT and Development (Build)"
              end)  AS `job_role_category`,
    core_contenttag_name  AS `job_role_name`,
    case when core_contenttag_id in (1254,1255,1260,1271,1267) then 1
              when core_contenttag_id in (1256,1269,1268,1270,1257) then 2
              when core_contenttag_id in (1265,1264,1266,1259,1263,1261,1262,1258) then 3
              end  AS `job_role_position`,
    COUNT(DISTINCT user_id ) AS `active_user_count`,
    SUM(activity_count_dim) AS `activity_count`
FROM data
GROUP BY 1,2,3,4
