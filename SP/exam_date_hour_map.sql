CREATE DEFINER=`ming`@`%` PROCEDURE `analytics`.`exam_date_hour_map`()
    MODIFIES SQL DATA
    DETERMINISTIC
BEGIN
truncate table exam_date_hour_map;

INSERT exam_date_hour_map(course_id,exam_subnet_id, hour_type,start,exam_durations,room_hours) 
select * from 
(
select distinct  course_id, exam_subnet_id,
case when  ed.exam_durations < 24 then 'regular' when ed.exam_durations <27 then  'extension' else NULL END as hour_type,
 start, ed.exam_durations, 
DATE_ADD(start, INTERVAL ed.exam_durations HOUR) as room_hours
 from os.exam_dates as exam_dates
left join analytics.exam_room_duration as ed on 1 =1 
where
    exam_dates.start >= (select min(exam_dates.start )from  os.exam_dates as  exam_dates where course_id = 21) 
      and ed.exam_durations <27 and course_id = 21
 )t ;
       
INSERT exam_date_hour_map(course_id,exam_subnet_id,hour_type,start,exam_durations,room_hours) 
select * from 
(
select distinct  course_id, exam_subnet_id,
case when  ed.exam_durations < 48 then 'regular' when ed.exam_durations <50 then  'extension' else NULL END as hour_type,
 start, ed.exam_durations, 
DATE_ADD(start, INTERVAL ed.exam_durations HOUR) as room_hours
 from os.exam_dates as exam_dates
left join analytics.exam_room_duration as ed on 1 =1 
where
    exam_dates.start >= (select min(exam_dates.start )from  os.exam_dates as exam_dates where course_id = 28 and 
    CAST(timestampdiff(second,exam_dates.start, exam_dates.end)/3600 AS UNSIGNED) = 50) 
      and ed.exam_durations <50 and course_id = 28 and   CAST(timestampdiff(second,exam_dates.start, exam_dates.end)/3600 AS UNSIGNED) = 50
) t;      
       
 END
