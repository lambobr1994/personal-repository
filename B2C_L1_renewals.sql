WITH core_examattempt_by_orderproduct_pdt AS (
        SELECT
            core_orderproduct.id AS orderproduct_id
            , CASE WHEN core_examattempt.exam_type = "awae" THEN "WEB-300"
              WHEN core_examattempt.exam_type = "ctp" THEN "CTP"
              WHEN core_examattempt.exam_type = "etbd"  THEN "PEN-300"
              WHEN core_examattempt.exam_type = "exp312"  THEN "EXP-312"
              WHEN core_examattempt.exam_type = "klr" THEN "KLCP"
              WHEN core_examattempt.exam_type = "pwk" THEN "PEN-200"
              WHEN core_examattempt.exam_type = "soc200"  THEN "SOC-200"
              WHEN core_examattempt.exam_type = "web200"  THEN "WEB-200"
              WHEN core_examattempt.exam_type = "wifu"  THEN "PEN-210"
              WHEN core_examattempt.exam_type = "wumed" THEN "EXP-301"
          ELSE core_examattempt.exam_type END AS course
            -- , COUNT(DISTINCT case when core_examattempt.status in (13) then usercontentaccess_ptr_id END) AS count_passed
            , COUNT(DISTINCT case when core_examattempt.status in (13, 14, 16, 1, 9) then usercontentaccess_ptr_id END) AS count_attempts
            -- , COUNT(DISTINCT case when core_examattempt.status in (0,3) then usercontentaccess_ptr_id END) AS count_scheduled
            , max(case when status in (13) then starts_at END) AS passed_date
            -- , max(case when status in (14,16) then starts_at END) AS failed_date
            , max(case when status in (0,3) then starts_at END) AS scheduled_date
            , MAX(CASE WHEN examattempt_status_name <> 'Scheduled' THEN starts_at END) AS last_exam_attempt_date
            , SUBSTRING_INDEX(GROUP_CONCAT(CASE WHEN starts_at IS NOT NULL AND examattempt_status_name <> 'Scheduled' THEN examattempt_status_name END ORDER BY starts_at DESC),',',1)   AS  last_exam_result
        FROM `offsec_platform`.`core_examattempt` AS `core_examattempt`
        LEFT JOIN `analytics`.`examattempt_status` AS `examattempt_status` ON `core_examattempt`.`status` = `examattempt_status`.`id`
        LEFT JOIN `offsec_platform`.`core_usercontentaccessthroughorders` AS `core_usercontentaccessthroughorders` ON `core_examattempt`.`usercontentaccess_ptr_id` = `core_usercontentaccessthroughorders`.`user_content_access_id`
        LEFT JOIN `offsec_platform`.`core_orderproduct` AS `core_orderproduct` ON `core_usercontentaccessthroughorders`.`order_product_id` = `core_orderproduct`.`id`
        GROUP BY 1,2

      )
  ,  core_latest_payment_by_order_id_pdt AS (select order_id, max(core_payment.id) as latest_payment_id_all,
    max(case when core_payment.payment_status = 1 then core_payment.id end) as latest_payment_id
      from  offsec_platform.core_orderpayment  AS core_orderpayment
      LEFT JOIN offsec_platform.core_payment  AS core_payment ON core_orderpayment.payment_id  = core_payment.id
      group by 1
 )
  ,  total_learningunits_pdt AS (with included_lu as
    (
      select lu_top.id AS course_id, lu_top.name as course_name, lu_top.code as course_code, lu_module.id as module_id, lu_module.learning_unit_type as module_learning_unit_type,
          lu1.id, lu1.name, lu1.learning_unit_type
      from core_learningunit lu_top
      join core_learningunitmap lum0 on lum0.top_level_learning_unit_id = lu_top.id and lum0.relation_type = 'hierarchical'
      join core_learningunit lu_module on lum0.secondary_learning_unit_id = lu_module.id and lu_module.learning_unit_type in ('book_module', 'video_module', 'lab')
      left join core_learningunitmap lum1 on lum1.primary_learning_unit_id = lu_module.id and lum1.relation_type = 'hierarchical'
      left join core_learningunit lu1 on lum1.secondary_learning_unit_id = lu1.id and lu1.status in ('published','archived')
      -- where lu_top.name = 'SOC-200'
     -- where lu_top.code = _course_code
          and (
              lu1.learning_unit_type in ('subsection', 'video', 'question')
              or lu1.learning_unit_type = 'host' and exists (select 1 from core_proof p where p.learning_unit_id = lu1.id)
              )

      union

      select lu_top.id AS course_id, lu_top.name as course_name, lu_top.code as course_code, lu_module.id as module_id, lu_module.learning_unit_type as module_learning_unit_type,
          lu2.id, lu2.name, lu2.learning_unit_type
      from core_learningunit lu_top
      join core_learningunitmap lum0 on lum0.top_level_learning_unit_id = lu_top.id and lum0.relation_type = 'hierarchical'
      join core_learningunit lu_module on lum0.secondary_learning_unit_id = lu_module.id and lu_module.learning_unit_type in ('book_module', 'video_module', 'lab')
      left join core_learningunitmap lum1 on lum1.primary_learning_unit_id = lu_module.id and lum1.relation_type = 'hierarchical'
      left join core_learningunit lu1 on lum1.secondary_learning_unit_id = lu1.id and lu1.status in ('published','archived')
      left join core_learningunitmap lum2 on lum2.primary_learning_unit_id = lu1.id and lum2.relation_type = 'hierarchical'
      left join core_learningunit lu2 on lum2.secondary_learning_unit_id = lu2.id and lu2.status in ('published','archived')
      -- where lu_top.name = 'SOC-200'
   --   where lu_top.code = _course_code
          and (
              lu2.learning_unit_type in ('subsection', 'video', 'question')
              or lu2.learning_unit_type = 'host' and exists (select 1 from core_proof p where p.learning_unit_id = lu2.id)
              )

      union

      select lu_top.id AS course_id, lu_top.name as course_name, lu_top.code as course_code, lu_module.id as module_id, lu_module.learning_unit_type as module_learning_unit_type,
          lu3.id, lu3.name, lu3.learning_unit_type
      from core_learningunit lu_top
      join core_learningunitmap lum0 on lum0.top_level_learning_unit_id = lu_top.id and lum0.relation_type = 'hierarchical'
      join core_learningunit lu_module on lum0.secondary_learning_unit_id = lu_module.id and lu_module.learning_unit_type in ('book_module', 'video_module', 'lab')
      left join core_learningunitmap lum1 on lum1.primary_learning_unit_id = lu_module.id and lum1.relation_type = 'hierarchical'
      left join core_learningunit lu1 on lum1.secondary_learning_unit_id = lu1.id and lu1.status in ('published','archived')
      left join core_learningunitmap lum2 on lum2.primary_learning_unit_id = lu1.id and lum2.relation_type = 'hierarchical'
      left join core_learningunit lu2 on lum2.secondary_learning_unit_id = lu2.id and lu2.status in ('published','archived')
      left join core_learningunitmap lum3 on lum3.primary_learning_unit_id = lu2.id and lum3.relation_type = 'hierarchical'
      left join core_learningunit lu3 on lum3.secondary_learning_unit_id = lu3.id and lu3.status in ('published','archived')
      -- where lu_top.name = 'SOC-200'
   --   where lu_top.code = _course_code
          and (
              lu3.learning_unit_type in ('subsection', 'video', 'question')
              or lu3.learning_unit_type = 'host' and exists (select 1 from core_proof p where p.learning_unit_id = lu3.id)
              )
    )

    , aggregates AS
    (
    select
        course_id,
        course_name,
        sum(case when module_learning_unit_type = 'book_module' then 1 else 0 end) as book_all,
        sum(case when module_learning_unit_type = 'video_module' then 1 else 0 end) as video_all,
        sum(case when module_learning_unit_type = 'lab' then 1 else 0 end) as lab_all,
        sum(case when module_learning_unit_type = 'book_module' and learning_unit_type = 'question' then 1 else 0 end) as exercise_all
      from included_lu
      where learning_unit_type is not null
      group by course_id, course_name
    )

    select course_id, course_name, book_all, video_all, lab_all, exercise_all, (coalesce(book_all,0)+coalesce(video_all,0)+coalesce(lab_all,0)+coalesce(exercise_all,0)) as total_all
    from aggregates
      )

    SELECT
    v_users.os_id  AS `v_users.os_id`,
        (DATE(v_users.last_login )) AS `v_users.last_login_date`,
    v_users.country_name  AS `v_users.country_name`,
    lms_mtd_with_unicorn_pdt.product_name  AS `lms_mtd_with_unicorn_pdt.product_name`,
    core_order.id  AS `core_order.id`,
    (CAST(latest_payment.payment_date  AS CHAR(19))) AS `latest_payment.payment_time`,
        (DATE(coalesce((DATE(latest_payment.updated_at )),(DATE(latest_payment.payment_date )) ,(DATE(lms_mtd_with_unicorn_pdt.order_date )) ) )) AS `lms_mtd_with_unicorn_pdt.mtd_date_date`,
    core_order.total  AS `core_order.total`,
    renewal_order.id  AS `renewal_order.id`,
    renewal_order_status.order_status_name  AS `renewal_order_status.order_status_name`,
        (DATE(renewal_order.order_due_date )) AS `renewal_order.order_due_date`,
    renewal_order.total  AS `renewal_order.total`,
        (DATE(core_usercontentaccess.start )) AS `core_usercontentaccess.start_date`,
        (DATE(core_usercontentaccess.end )) AS `core_usercontentaccess.end_date`,
    course_selected.name  AS `course_selected.name`,
    ROUND(COALESCE(SUM(case when core_progressaggregate.learning_path_type  = "book_module" then core_progressaggregate.completed else NULL end ), 0)*100/total_learningunits_pdt.book_all)  AS `%book`,
    ROUND(COALESCE(SUM(case when core_progressaggregate.learning_path_type  = "video_module" then core_progressaggregate.completed else NULL end ), 0)*100/total_learningunits_pdt.video_all)  AS `%video`,
    ROUND(COALESCE(SUM(case when core_progressaggregate.learning_path_type  = "book_module" then core_progressaggregate.correct_answers else NULL end ), 0)*100/total_learningunits_pdt.exercise_all)  AS `%exercise`,
    ROUND(COALESCE(SUM(case when core_progressaggregate.learning_path_type  = "lab" then core_progressaggregate.completed else NULL end ), 0)*100/total_learningunits_pdt.lab_all)  AS `%lab`,
    ROUND(COALESCE(SUM(core_progressaggregate.completed ), 0)*100/total_learningunits_pdt.total_all)  AS `%total`
    , count_attempts AS `#exam_attempts`
    , DATE(passed_date) AS exam_passed_date
    , DATE(scheduled_date) AS next_exam_date
    -- , count_passed
    -- , count_scheduled
    -- , DATE(failed_date) AS failed_date
    , DATE(last_exam_attempt_date) AS last_exam_attempt_date
    , last_exam_result
    
    
    
    -- COUNT(DISTINCT case when core_examattempt_by_orderproduct_pdt.status in (13, 14, 16, 1, 9) then core_examattempt_by_orderproduct_pdt.usercontentaccess_ptr_id END ) AS `core_examattempt_by_orderproduct_pdt.count_attempts`,
    -- COUNT(DISTINCT case when core_examattempt_by_orderproduct_pdt.status in (13) then core_examattempt_by_orderproduct_pdt.usercontentaccess_ptr_id END ) AS `core_examattempt_by_orderproduct_pdt.count_passed`,
    -- COUNT(DISTINCT case when core_examattempt_by_orderproduct_pdt.status in (0, 3) then core_examattempt_by_orderproduct_pdt.usercontentaccess_ptr_id END ) AS `core_examattempt_by_orderproduct_pdt.exam_scheduled`,
    -- max(case when core_examattempt_by_orderproduct_pdt.status in (14,16) then DATE(core_examattempt_by_orderproduct_pdt.starts_at) END)  AS `core_examattempt_by_orderproduct_pdt.date_failed`,
    -- max(case when core_examattempt_by_orderproduct_pdt.status in (13) then DATE(core_examattempt_by_orderproduct_pdt.starts_at) END)   AS `core_examattempt_by_orderproduct_pdt.date_passed`,
    -- max(case when core_examattempt_by_orderproduct_pdt.status in (0,3) then DATE(core_examattempt_by_orderproduct_pdt.starts_at) END)  AS `core_examattempt_by_orderproduct_pdt.date_scheduled`
FROM analytics.looker_lms_mtd_with_unicorn  AS lms_mtd_with_unicorn_pdt
LEFT JOIN offsec_platform.core_order  AS core_order ON case when lms_mtd_with_unicorn_pdt.data_source in ('B2C','LMS_licenses','LMS_credits')
                  then  core_order.id
                  else  core_order.tmp_unicorn_purchase_id  END
                  = lms_mtd_with_unicorn_pdt.order_id
LEFT JOIN offsec_platform.v_users  AS v_users ON core_order.user_id = v_users.id
LEFT JOIN offsec_platform.core_order  AS renewal_order ON core_order.id = renewal_order.parent_order_id
LEFT JOIN analytics.order_status  AS renewal_order_status ON renewal_order.order_status = renewal_order_status.id
LEFT JOIN core_latest_payment_by_order_id_pdt ON core_order.id = core_latest_payment_by_order_id_pdt.order_id
LEFT JOIN offsec_platform.core_payment  AS latest_payment ON core_latest_payment_by_order_id_pdt.latest_payment_id  = latest_payment.id
LEFT JOIN offsec_platform.core_orderproduct  AS core_orderproduct ON core_order.id = core_orderproduct.order_id
LEFT JOIN offsec_platform.core_productvariant  AS core_productvariant ON core_orderproduct.product_variant_id = core_productvariant.id
LEFT JOIN offsec_platform.core_product  AS core_product ON core_productvariant.product_id = core_product.id
LEFT JOIN offsec_platform.core_usercontentaccessthroughorders  AS core_usercontentaccessthroughorders ON core_orderproduct.id = core_usercontentaccessthroughorders.order_product_id
LEFT JOIN offsec_platform.core_usercontentaccess  AS core_usercontentaccess ON core_usercontentaccessthroughorders.user_content_access_id = core_usercontentaccess.id
LEFT JOIN offsec_platform.core_subscriptioncourseselection  AS core_subscriptioncourseselection ON core_usercontentaccessthroughorders.user_content_access_id = core_subscriptioncourseselection.user_content_access_id
LEFT JOIN offsec_platform.core_product  AS course_selected ON core_subscriptioncourseselection.product_id = course_selected.id
LEFT JOIN offsec_platform.core_learningunit  AS root_learningunit ON COALESCE(course_selected.name,core_product.name) = root_learningunit.name
LEFT JOIN offsec_platform.core_learningunitmap  AS lum_section ON lum_section.top_level_learning_unit_id =root_learningunit.id
LEFT JOIN offsec_platform.core_progressaggregate  AS core_progressaggregate ON v_users.id = core_progressaggregate.user_id AND lum_section.secondary_learning_unit_id = core_progressaggregate.learning_path_id and lum_section.relation_type = 'hierarchical'
LEFT JOIN total_learningunits_pdt ON root_learningunit.id = total_learningunits_pdt.course_id

LEFT JOIN core_examattempt_by_orderproduct_pdt ON core_orderproduct.id = core_examattempt_by_orderproduct_pdt.orderproduct_id AND core_examattempt_by_orderproduct_pdt.course = course_selected.name
WHERE (lms_mtd_with_unicorn_pdt.data_source ) IN ('B2C', 'unicorn_platform') AND (coalesce((DATE(latest_payment.updated_at )),(DATE(latest_payment.payment_date )) ,(DATE(lms_mtd_with_unicorn_pdt.order_date )) ) ) >= (TIMESTAMP('2021-09-01')) AND (lms_mtd_with_unicorn_pdt.product_name ) = 'Learn One' AND (NOT (case when v_users.email like '%evozon%' or
            v_users.email like '%offensive-security.com%' or
            v_users.email like '%accelone%'
            then True else False end  ) OR (case when v_users.email like '%evozon%' or
            v_users.email like '%offensive-security.com%' or
            v_users.email like '%accelone%'
            then True else False end  ) IS NULL) AND (course_selected.name ) IS NOT NULL AND ((renewal_order_status.order_status_name ) = 'Pending Payment')
            -- AND os_id = 14866
GROUP BY
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,13,14,15

