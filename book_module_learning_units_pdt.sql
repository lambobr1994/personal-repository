view: book_module_learning_units_pdt {
  derived_table: {
    sql:
    -- subsections
    SELECT
        course.id  AS `course.id`,
        course.name  AS `course.name`,
        book_module.id  AS `book_module.id`,
        book_module.name  AS `book_module.name`,
        book_module.learning_unit_type  AS `book_module.learning_unit_type`,
        section.id AS `learning_unit_id`,
        section.name AS `learning_unit_name`
    FROM offsec_platform.core_learningunit  AS book_module
    INNER JOIN offsec_platform.core_learningunitmap  AS book_map ON book_module.id =book_map.secondary_learning_unit_id and   book_module.learning_unit_type = 'book_module' and  book_map.relation_type = "hierarchical"
    INNER JOIN offsec_platform.core_learningunit  AS course ON course.id =book_map.top_level_learning_unit_id
    INNER JOIN offsec_platform.core_learningunitmap  AS section_map ON section_map.primary_learning_unit_id = book_map.secondary_learning_unit_id and section_map.relation_type = "hierarchical"
    INNER JOIN offsec_platform.core_learningunit  AS section ON section.id =section_map.secondary_learning_unit_id  and section.learning_unit_type = 'section' -- and  section.status = "published"
    INNER JOIN offsec_platform.core_learningunitmap  AS subsection_map ON subsection_map.primary_learning_unit_id = section_map.secondary_learning_unit_id and subsection_map.relation_type = "hierarchical"
    INNER JOIN offsec_platform.core_learningunit  AS subsection ON subsection.id =subsection_map.secondary_learning_unit_id  and subsection.learning_unit_type = 'subsection' -- and  subsection.status = "published"

    UNION

    SELECT
        course.id  AS `course.id`,
        course.name  AS `course.name`,
        book_module.id  AS `book_module.id`,
        book_module.name  AS `book_module.name`,
        book_module.learning_unit_type  AS `book_module.learning_unit_type`,
        subsection.id AS `learning_unit_id`,
        subsection.name AS `learning_unit_name`
    FROM offsec_platform.core_learningunit  AS book_module
    INNER JOIN offsec_platform.core_learningunitmap  AS book_map ON book_module.id =book_map.secondary_learning_unit_id and   book_module.learning_unit_type = 'book_module' and  book_map.relation_type = "hierarchical"
    INNER JOIN offsec_platform.core_learningunit  AS course ON course.id =book_map.top_level_learning_unit_id
    INNER JOIN offsec_platform.core_learningunitmap  AS section_map ON section_map.primary_learning_unit_id = book_map.secondary_learning_unit_id and section_map.relation_type = "hierarchical"
    INNER JOIN offsec_platform.core_learningunit  AS section ON section.id =section_map.secondary_learning_unit_id  and section.learning_unit_type = 'section' -- and  section.status = "published"
    INNER JOIN offsec_platform.core_learningunitmap  AS subsection_map ON subsection_map.primary_learning_unit_id = section_map.secondary_learning_unit_id and subsection_map.relation_type = "hierarchical"
    INNER JOIN offsec_platform.core_learningunit  AS subsection ON subsection.id =subsection_map.secondary_learning_unit_id  and subsection.learning_unit_type = 'subsection' -- and  subsection.status = "published"

    UNION

    -- video

    SELECT
        course.id  AS `course.id`,
        course.name  AS `course.name`,
        book_module.id  AS `book_module.id`,
        book_module.name  AS `book_module.name`,
        book_module.learning_unit_type  AS `book_module.learning_unit_type`,
        section.id AS `learning_unit_id`,
        section.name AS `learning_unit_name`
    FROM offsec_platform.core_learningunit  AS book_module
    INNER JOIN offsec_platform.core_learningunitmap  AS book_map ON book_module.id =book_map.secondary_learning_unit_id and   book_module.learning_unit_type = 'video_module' and  book_map.relation_type = "hierarchical"
    INNER JOIN offsec_platform.core_learningunit  AS course ON course.id =book_map.top_level_learning_unit_id
    INNER JOIN offsec_platform.core_learningunitmap  AS section_map ON section_map.primary_learning_unit_id = book_map.secondary_learning_unit_id  and section_map.relation_type = "hierarchical"
    INNER JOIN offsec_platform.core_learningunit  AS section ON section.id =section_map.secondary_learning_unit_id  and section.learning_unit_type  = 'video_section' -- and  section.status = "published"
    INNER JOIN offsec_platform.core_learningunitmap  AS subsection_map ON subsection_map.primary_learning_unit_id = section_map.secondary_learning_unit_id  and subsection_map.relation_type = "hierarchical"
    INNER JOIN offsec_platform.core_learningunit  AS subsection ON subsection.id =subsection_map.secondary_learning_unit_id  and subsection.learning_unit_type = 'video' -- and  subsection.status = "published"

    UNION

    SELECT
        course.id  AS `course.id`,
        course.name  AS `course.name`,
        book_module.id  AS `book_module.id`,
        book_module.name  AS `book_module.name`,
        book_module.learning_unit_type  AS `book_module.learning_unit_type`,
        subsection.id AS `learning_unit_id`,
        subsection.name AS `learning_unit_name`
    FROM offsec_platform.core_learningunit  AS book_module
    INNER JOIN offsec_platform.core_learningunitmap  AS book_map ON book_module.id =book_map.secondary_learning_unit_id and   book_module.learning_unit_type = 'video_module' and  book_map.relation_type = "hierarchical"
    INNER JOIN offsec_platform.core_learningunit  AS course ON course.id =book_map.top_level_learning_unit_id
    INNER JOIN offsec_platform.core_learningunitmap  AS section_map ON section_map.primary_learning_unit_id = book_map.secondary_learning_unit_id  and section_map.relation_type = "hierarchical"
    INNER JOIN offsec_platform.core_learningunit  AS section ON section.id =section_map.secondary_learning_unit_id  and section.learning_unit_type  = 'video_section' -- and  section.status = "published"
    INNER JOIN offsec_platform.core_learningunitmap  AS subsection_map ON subsection_map.primary_learning_unit_id = section_map.secondary_learning_unit_id  and subsection_map.relation_type = "hierarchical"
    INNER JOIN offsec_platform.core_learningunit  AS subsection ON subsection.id =subsection_map.secondary_learning_unit_id  and subsection.learning_unit_type = 'video' -- and  subsection.status = "published"

    UNION

    -- challenges

    SELECT
        course.id  AS `course.id`,
        course.name  AS `course.name`,
        question_module.id  AS `book_module.id`,
        question_module.name  AS `book_module.name`,
        question_module.learning_unit_type  AS `book_module.learning_unit_type`,
        question.id AS `learning_unit_id`,
        question.name AS `learning_unit_name`
    FROM offsec_platform.core_learningunit  AS question_module
      INNER JOIN offsec_platform.core_learningunitmap  AS book_map ON question_module.id =book_map.secondary_learning_unit_id and   question_module.learning_unit_type = 'book_module' and  book_map.relation_type = "hierarchical"
      INNER JOIN offsec_platform.core_learningunit  AS course ON course.id =book_map.top_level_learning_unit_id
      INNER JOIN offsec_platform.core_learningunitmap  AS section_map ON section_map.primary_learning_unit_id = book_map.secondary_learning_unit_id and section_map.relation_type = "hierarchical"
      INNER JOIN offsec_platform.core_learningunitmap  AS subsection_map ON subsection_map.primary_learning_unit_id = section_map.secondary_learning_unit_id and subsection_map.relation_type = "hierarchical"
      INNER JOIN offsec_platform.core_learningunit  AS subsection ON subsection.id =subsection_map.secondary_learning_unit_id  and subsection.learning_unit_type = 'challenge'
      INNER JOIN offsec_platform.core_learningunitmap  AS question_map ON question_map.primary_learning_unit_id = subsection_map.secondary_learning_unit_id and question_map.relation_type = "hierarchical"
      INNER JOIN offsec_platform.core_learningunit  AS question ON question.id =question_map.secondary_learning_unit_id  and question.learning_unit_type = 'question'

    UNION

    SELECT
        course.id  AS `course.id`,
        course.name  AS `course.name`,
        question_module.id  AS `book_module.id`,
        question_module.name  AS `book_module.name`,
        question_module.learning_unit_type  AS `book_module.learning_unit_type`,
        subsection.id AS `learning_unit_id`,
        subsection.name AS `learning_unit_name`
    FROM offsec_platform.core_learningunit  AS question_module
      INNER JOIN offsec_platform.core_learningunitmap  AS book_map ON question_module.id =book_map.secondary_learning_unit_id and   question_module.learning_unit_type = 'book_module' -- and  book_map.relation_type = "hierarchical"
      INNER JOIN offsec_platform.core_learningunit  AS course ON course.id =book_map.top_level_learning_unit_id
      INNER JOIN offsec_platform.core_learningunitmap  AS section_map ON section_map.primary_learning_unit_id = book_map.secondary_learning_unit_id  and section_map.relation_type = "hierarchical"
      INNER JOIN offsec_platform.core_learningunitmap  AS subsection_map ON subsection_map.primary_learning_unit_id = section_map.secondary_learning_unit_id and subsection_map.relation_type = "hierarchical"
      INNER JOIN offsec_platform.core_learningunit  AS subsection ON subsection.id =subsection_map.secondary_learning_unit_id  and subsection.learning_unit_type = 'challenge'
      INNER JOIN offsec_platform.core_learningunitmap  AS question_map ON question_map.primary_learning_unit_id = subsection_map.secondary_learning_unit_id and question_map.relation_type = "hierarchical"
      INNER JOIN offsec_platform.core_learningunit  AS question ON question.id =question_map.secondary_learning_unit_id  and question.learning_unit_type = 'question'
       ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: course_id {
    type: number
    sql: ${TABLE}.`course.id` ;;
  }

  dimension: course_name {
    type: string
    sql: ${TABLE}.`course.name` ;;
  }

  dimension: book_module_id {
    type: number
    value_format_name: decimal_0
    sql: ${TABLE}.`book_module.id` ;;
  }

  dimension: book_module_name {
    type: string
    sql: ${TABLE}.`book_module.name` ;;
  }

  dimension: book_module_learning_unit_type {
    type: string
    sql: ${TABLE}.`book_module.learning_unit_type` ;;
  }

  dimension: learning_unit_id {
    type: number
    sql: ${TABLE}.`subsection_id` ;;
  }

  dimension: learning_unit_name {
    type: string
    sql: ${TABLE}.`learning_unit_name` ;;
  }



  # measure: subsection_count_distinct_id_sum {
  #   type: sum
  #   value_format_name: decimal_0
  #   sql: ${TABLE}.`subsection.count_distinct_id` ;;
  # }


  set: detail {
    fields: [
      course_id,
      course_name,
      book_module_id,
      book_module_name,
      book_module_learning_unit_type,
    ]
  }
}
