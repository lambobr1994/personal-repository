explore: lms_mtd_pdt_with_unicorn {
  join: v_users {
    relationship: one_to_one
    sql_on: ${lms_mtd_pdt_with_unicorn.user_id} = ${v_users.id} ;;
  }

  join: core_order {
    type: left_outer
    relationship: one_to_one
    sql_on: ${lms_mtd_pdt_with_unicorn.order_id} = ${core_order.id}   ;;
  }

  join: core_latest_payment_by_order_id_pdt {
    relationship: one_to_one
    sql_on: ${core_order.id} = ${core_latest_payment_by_order_id_pdt.order_id}  ;;
  }

  join: latest_payment {
    from:  core_payment
    relationship: one_to_one
    sql_on: ${core_latest_payment_by_order_id_pdt.latest_payment_id}  = ${latest_payment.id} ;;
  }

  join: first_paid_order_pdt {
    type: left_outer
    relationship: one_to_one
    sql_on: ${lms_mtd_pdt_with_unicorn.order_id} = ${first_paid_order_pdt.first_paid_order_id}   ;;
  }

  join: accounts {
    type: left_outer
    relationship: one_to_one
    sql_on: ${lms_mtd_pdt_with_unicorn.account_id} = ${accounts.id}   ;;
  }
}
