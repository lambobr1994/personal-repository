-- LMS data

-- B2C orders
SELECT
    core_order.id as order_id, core_order.id as record_id, 1 as quantity,-- polymorphic id depending on datasource
    core_orderproduct.id as orderproduct_id,  core_order.user_id as user_id, NULL as account_id,     'B2C' as 'data_source',
    `core_order`.`order_due_date` AS `order_date`,
    `core_product`.`name` AS `product_name`,
    core_productvariant.id as productvariant_id,
    coalesce(core_orderproduct.price_value,price.value) AS total,
    null AS `unicorn_purchase_type_name`,
    null AS `unicorn_is_returning`
FROM offsec_platform.core_order  AS core_order
LEFT JOIN offsec_platform.core_orderproduct  AS core_orderproduct ON core_order.id =core_orderproduct.order_id
LEFT JOIN offsec_platform.core_productvariant  AS core_productvariant ON core_orderproduct.product_variant_id = core_productvariant.id
LEFT JOIN offsec_platform.core_product  AS core_product ON core_productvariant.product_id  =core_product.id
LEFT JOIN offsec_platform.core_price  AS price ON core_productvariant.price_id  =price.id
LEFT JOIN offsec_platform.accountorderslicenses  AS accountorderslicenses ON core_order.id  = accountorderslicenses.order_id
LEFT JOIN offsec_platform.v_users_lite  AS v_users ON core_order.user_id =v_users.id
WHERE (core_order.order_due_date ) >= (TIMESTAMP('2019-01-01 01:00'))
AND (core_order.total ) > 0
and (core_order.order_status = 1)
AND core_product.name not in ('PG Play','Flex','Kali Linux Revealed')
AND accountorderslicenses.id is NULL
AND ( `core_order`.`tmp_unicorn_purchase_id` IS NULL
OR (`core_product`.`name` = ('Learn One') AND `core_order`.`tmp_unicorn_purchase_id` IS NOT NULL)
OR (`core_product`.`name` = ('Learn Fundamentals') AND `core_order`.`tmp_unicorn_purchase_id` IS NOT NULL))
AND `v_users`.`domain` NOT IN ('evozon.com', 'offensive-security.com', 'offsec.com')


UNION ALL

--  Credits
select 
    AOL.order_id as order_id , AOL.id as record_id, 1 as quantity, -- polymorphic id , Accountordercredits.id for credits
    NULL as orderproduct_id,`accountadmin`.`id` AS user_id,  a.id as account_id,'LMS_credits' as 'data_source', AOL.created_at as order_date, 'Flex' as product_name, NULL as productvariant_id, price as total,
    -- salesforce.closedate AS order_date
    -- select  date(AOL.created_at) as date,P.name as product_namea, sum( price)
    null AS `unicorn_purchase_type_name`,
    null AS `unicorn_is_returning`
from accountorderscredits AOL
left join accounts a on AOL.account_id = a.id
left join `offsec_platform`.`accountusers` AS `accountusers` ON `AOL`.`account_id` = `accountusers`.`account_id` AND CAST(`accountusers`.`user_account_role_id` AS SIGNED) = 2
left join `offsec_platform`.`v_users_lite` AS `accountadmin` ON `accountusers`.`user_id` = `accountadmin`.`id`
-- left join `salesforce`.`Opportunity` AS `salesforce` ON `AOL`.`sales_force_op_id` = `salesforce`.`global_id`
-- left join core_productvariant PV on PV.id = AOL.productvariant_id
-- left join core_product P on P.id = PV.product_id
-- left join core_productfamily PF on P.product_family_id= PF.id
where -- AOL.order_id is NULL and
-- PV.code in ('Course & lab-basic-OTL-Learn Unlimited', 'Course & lab-basic-OTL-Learn One') and
AOL.created_at > '2019-01-01'and a.id  not in (24613, 24621,24628,24679,24678,24737   )  and a.parent_account_id  not in (24620,9009,9015,9017 ,24612)  and NOT a.account_name like "%LMS Demo%"  and NOT a.account_name like "%Offensive-security 123%" and AOL.unicorn_purchase_id is NULL

and NOT a.account_name like "%offsec test%"
AND (`accountadmin`.`domain` NOT LIKE '%offensive-security%' AND `accountadmin`.`domain` NOT LIKE '%evozon%' OR `accountadmin`.`domain` IS NULL) -- SAP has Lori@offsec.com as admin, so cannot exclude offsec.com
AND (AOL.`is_paid` <> "false" )

UNION ALL

-- licenses
select -- account_lic orders where there's no unicorn purchase-ids
    AOL.order_id as order_id ,  AOL.id as record_id, AOL.quantity as quantity, -- polymorphic id , Accountorderlicense.id for credits
    NULL as orderproduct_id,   accountadmin.user_id  as user_id, a.id as account_id,
    'LMS_licenses' as 'data_source', AOL.created_at as order_date, P.name as product_name,  PV.id as productvariant_id ,-- PV.`code` AS `code`,
    price as total,
    -- salesforce.closedate AS order_date
    -- select  date(AOL.created_at) as date,P.name as product_namea, sum( price)
    null AS `unicorn_purchase_type_name`,
    null AS `unicorn_is_returning`
from accountorderslicenses AOL
left join accounts a on AOL.account_id = a.id
left join offsec_platform.accountusers AS accountadmin ON AOL.account_id = accountadmin.account_id AND CAST(`accountadmin`.`user_account_role_id` AS SIGNED) = 2
left join offsec_platform.v_users_lite AS v_users ON accountadmin.user_id = v_users.id
left join core_productvariant PV on PV.id = AOL.productvariant_id
left join core_product P on P.id = PV.product_id
left join core_productfamily PF on P.product_family_id= PF.id
-- left join `salesforce`.`Opportunity` AS `salesforce` ON `AOL`.`sales_force_op_id` = `salesforce`.`global_id`
where -- AOL.order_id is   NULL and
-- PV.code in ('Course & lab-basic-OTL-Learn Unlimited', 'Course & lab-basic-OTL-Learn One') and
AOL.created_at >= '2019-01-01'and a.id  not in (24613, 24621,24628 ,24663,24679,24678,24737  )and a.parent_account_id  not in (24620,9009,9015,9017,24612 ) and AOL.unicorn_purchase_id is NULL  and NOT a.account_name like "%LMS Demo%"  and NOT a.account_name like "%Offensive-security 123%"
and NOT a.account_name like "%offsec test%" --     and AOL.order_id = 120994
AND (`v_users`.`domain` NOT LIKE '%offensive-security%' AND  `v_users`.`domain` NOT LIKE '%offsec.com%' AND `v_users`.`domain` NOT LIKE '%evozon%' OR `v_users`.`domain` IS NULL)
AND (AOL.`is_paid` <> "false" )

union all

      -- UNICORN data

SELECT
    purchases.id AS `order_id`, -- only used for joining to unicorn tables; use record_id for counting
    CONCAT('u-',purchases.id) AS `record_id`,
    1 AS `quantity`,
    null AS `orderproduct_id`,
    users.id AS `user_id`,
    null AS `account_id`,
    CASE WHEN  purchases.created_by in (2,5,6,7,8) THEN 'unicorn_salesforce' ELSE 'unicorn_platform' END AS `data_source`,
    purchases.purchase_payment  AS `order_date`,
    case  when   purchases.type = "0" then courses.shotcutname
    when   purchases.type = "19" and purchases.voucher_id = 0 and purchases.price > 0 then "PWK365(B2C)"
    when   purchases.type =  "7"  and v.pid is not NULL then "PWK365(B2B)"
    when   purchases.type =  "12" and payment_links.description = 'PWK365 upgrade' then "PWK365(UPG)"
    when  purchases.type =  "4" then "Certification"
    when   purchases.type =  "7" then "Voucher"
    when   purchases.type in (  "9","10","11") then "Proving_Grounds"
    when   purchases.type in (  "6","8","14","15") then "Updates/Upgrades (Course/Lab)"
    when   purchases.type in (  "13","17") then "Flex"
    --  when   purchases.type in (  "16") then "Academy"
    -- when   purchases.type = "1" then "LiveCourse"
    when purchases.type = "3" or purchases.type = "2"  then "Lab"
    else "Other" end  AS `product_name`,
    null AS productvariant_id,
    COALESCE(SUM(purchases.price ), 0) AS `total`,
    purchase_type.name AS `unicorn_purchase_type_name`,
    CASE WHEN purchases.is_return_user THEN 'returning' ELSE 'new' END AS `unicorn_is_returning`
FROM os_looker.purchases  AS purchases
LEFT JOIN os_looker.purchase_type on purchases.type = purchase_type.id
LEFT JOIN os_looker.users  AS users ON purchases.user_id = users.id
LEFT JOIN os_looker.courses  AS courses ON purchases.course_id = courses.id
LEFT JOIN os_looker.payment_links AS payment_links on  purchases.id =payment_links.purchase_id AND purchases.type = 12
left join (SELECT
purchases.id  AS `pid`
FROM os_looker.voucher  AS voucher
LEFT JOIN os_looker.purchases  AS purchases ON voucher.purchase_id = purchases.id
WHERE (purchases.purchase_payment  IS NOT NULL) AND (voucher.purchase_type = 19)) as v on v.pid = purchases.id and purchases.type = 7
LEFT JOIN os_looker.purchase_status  AS purchase_status ON purchases.status = purchase_status.id
WHERE
((users.email1 NOT LIKE '%offensive-security%' AND users.email1 NOT LIKE '%offsec%') OR users.email1 IS NULL)
AND (purchases.purchase_payment  IS NOT NULL) AND NOT (purchase_status.name) IN ("Refunded", "Deleted")
GROUP BY 1,2,3,4,5,6,8,9,10,12,13
HAVING NOT (COALESCE(SUM(purchases.price ), 0) IS NULL)

UNION ALL

SELECT
    a.id AS `order_id`,
    a.id AS `record_id`,
    1 AS `quantity`,
    null AS `orderproduct_id`,
    null AS `user_id`,
    null AS `account_id`,
    a.data_source AS `data_source`,
    a.purchase_payment_date AS `order_date`,
    a.course_labs_grouped AS `product_name`,
    null AS productvariant_id,
    a.price AS `total`,
    a.purchase_type_name AS `unicorn_purchase_type_name`,
    CASE WHEN purchases.is_return_user THEN 'returning' ELSE 'new' END AS `unicorn_is_returning`
FROM analytics.manual_deals AS a
LEFT JOIN os_looker.purchases  AS purchases ON a.id = purchases.id
