view: lms_mtd_pdt_with_unicorn {
  derived_table: {
    sql:

    -- LMS data

     -- B2C orders
      SELECT
        core_order.id as order_id, core_order.id as record_id, 1 as quantity,-- polymorphic id depending on datasource
        core_orderproduct.id as orderproduct_id,  core_order.user_id as user_id, NULL as account_id,     'B2C' as 'data_source',
       `core_order`.`order_due_date` AS `order_date`,
       `core_product`.`name` AS `product_name`,
       core_productvariant.id as productvariant_id,
       coalesce(core_orderproduct.price_value,price.value) AS total,
      'lms' AS `main_source`,
      null  AS `unicorn_puchase_id`,
      null AS `unicorn_purchase_type_name`,
      null AS `unicorn_purchase_status_name`,
      null AS `unicorn_is_returning`
      FROM offsec_platform.core_order  AS core_order
      LEFT JOIN offsec_platform.core_orderproduct  AS core_orderproduct ON core_order.id =core_orderproduct.order_id
      LEFT JOIN offsec_platform.core_productvariant  AS core_productvariant ON core_orderproduct.product_variant_id = core_productvariant.id
      LEFT JOIN offsec_platform.core_product  AS core_product ON core_productvariant.product_id  =core_product.id
      LEFT JOIN offsec_platform.core_price  AS price ON core_productvariant.price_id  =price.id
      LEFT JOIN offsec_platform.accountorderslicenses  AS accountorderslicenses ON core_order.id  = accountorderslicenses.order_id
      LEFT JOIN offsec_platform.v_users_lite  AS v_users ON core_order.user_id =v_users.id
      WHERE (core_order.order_due_date ) >= (TIMESTAMP('2019-01-01 01:00'))
      AND (core_order.total ) > 0
      and (core_order.order_status = 1)
      AND core_product.name not in ('PG Play','Flex','Kali Linux Revealed')
      AND accountorderslicenses.id is NULL
      AND ( `core_order`.`tmp_unicorn_purchase_id` IS NULL
      OR (`core_product`.`name` = ('Learn One') AND `core_order`.`tmp_unicorn_purchase_id` IS NOT NULL)
      OR (`core_product`.`name` = ('Learn Fundamentals') AND `core_order`.`tmp_unicorn_purchase_id` IS NOT NULL))
      AND `v_users`.`domain` NOT IN ('evozon.com', 'offensive-security.com', 'offsec.com')


      UNION ALL

      --  Credits
      select AOL.order_id as order_id , AOL.id as record_id, 1 as quantity, -- polymorphic id , Accountordercredits.id for credits
      NULL as orderproduct_id,`accountadmin`.`id` AS user_id,  a.id as account_id,'LMS_credits' as 'data_source', AOL.created_at as order_date, 'Flex' as product_name, NULL as productvariant_id, price as total,
      -- salesforce.closedate AS order_date
      -- select  date(AOL.created_at) as date,P.name as product_namea, sum( price)
      'lms' AS `main_source`,
      null  AS `unicorn_puchase_id`,
      null AS `unicorn_purchase_type_name`,
      null AS `unicorn_purchase_status_name`,
      null AS `unicorn_is_returning`
      from accountorderscredits AOL
      left join accounts a on AOL.account_id = a.id
      left join `offsec_platform`.`accountusers` AS `accountusers` ON `AOL`.`account_id` = `accountusers`.`account_id` AND CAST(`accountusers`.`user_account_role_id` AS SIGNED) = 2
      left join `offsec_platform`.`v_users_lite` AS `accountadmin` ON `accountusers`.`user_id` = `accountadmin`.`id`
      -- left join `salesforce`.`Opportunity` AS `salesforce` ON `AOL`.`sales_force_op_id` = `salesforce`.`global_id`
      -- left join core_productvariant PV on PV.id = AOL.productvariant_id
      -- left join core_product P on P.id = PV.product_id
      -- left join core_productfamily PF on P.product_family_id= PF.id
      where -- AOL.order_id is NULL and
      -- PV.code in ('Course & lab-basic-OTL-Learn Unlimited', 'Course & lab-basic-OTL-Learn One') and
      AOL.created_at > '2019-01-01'and a.id  not in (24613, 24621,24628,24679,24678,24737   )  and a.parent_account_id  not in (24620,9009,9015,9017 ,24612)  and NOT a.account_name like "%LMS Demo%"  and NOT a.account_name like "%Offensive-security 123%" and AOL.unicorn_purchase_id is NULL

      and NOT a.account_name like "%offsec test%"
      AND (`accountadmin`.`domain` NOT LIKE '%offensive-security%' AND `accountadmin`.`domain` NOT LIKE '%evozon%' OR `accountadmin`.`domain` IS NULL) -- SAP has Lori@offsec.com as admin, so cannot exclude offsec.com
      AND (AOL.`is_paid` <> "false" )

      UNION ALL

      -- licenses
      select -- account_lic orders where there's no unicorn purchase-ids
      AOL.order_id as order_id ,  AOL.id as record_id, AOL.quantity as quantity, -- polymorphic id , Accountorderlicense.id for credits
      NULL as orderproduct_id,   accountadmin.user_id  as user_id, a.id as account_id,
      'LMS_licenses' as 'data_source', AOL.created_at as order_date, P.name as product_name,  PV.id as productvariant_id ,-- PV.`code` AS `code`,
      price as total,
      -- salesforce.closedate AS order_date
      -- select  date(AOL.created_at) as date,P.name as product_namea, sum( price)
      'lms' AS `main_source`,
      null  AS `unicorn_puchase_id`,
      null AS `unicorn_purchase_type_name`,
      null AS `unicorn_purchase_status_name`,
      null AS `unicorn_is_returning`
      from accountorderslicenses AOL
      left join accounts a on AOL.account_id = a.id
      left join offsec_platform.accountusers AS accountadmin ON AOL.account_id = accountadmin.account_id AND CAST(`accountadmin`.`user_account_role_id` AS SIGNED) = 2
      left join offsec_platform.v_users_lite AS v_users ON accountadmin.user_id = v_users.id
      left join core_productvariant PV on PV.id = AOL.productvariant_id
      left join core_product P on P.id = PV.product_id
      left join core_productfamily PF on P.product_family_id= PF.id
      -- left join `salesforce`.`Opportunity` AS `salesforce` ON `AOL`.`sales_force_op_id` = `salesforce`.`global_id`
      where -- AOL.order_id is   NULL and
      -- PV.code in ('Course & lab-basic-OTL-Learn Unlimited', 'Course & lab-basic-OTL-Learn One') and
      AOL.created_at >= '2019-01-01'and a.id  not in (24613, 24621,24628 ,24663,24679,24678,24737  )and a.parent_account_id  not in (24620,9009,9015,9017,24612 ) and AOL.unicorn_purchase_id is NULL  and NOT a.account_name like "%LMS Demo%"  and NOT a.account_name like "%Offensive-security 123%"
      and NOT a.account_name like "%offsec test%" --     and AOL.order_id = 120994
      AND (`v_users`.`domain` NOT LIKE '%offensive-security%' AND  `v_users`.`domain` NOT LIKE '%offsec.com%' AND `v_users`.`domain` NOT LIKE '%evozon%' OR `v_users`.`domain` IS NULL)
      AND (AOL.`is_paid` <> "false" )

      union all

      -- UNICORN data

      SELECT
        null AS `order_id`,
        null AS `record_id`,
        1 AS `quantity`,
        null AS `orderproduct_id`,
        users.id AS `user_id`,
        null AS `account_id`,
        CASE WHEN  purchases.created_by in (2,5,6,7,8) THEN 'salesforce' ELSE 'platform' END AS `data_source`,
        purchases.purchase_payment  AS `order_date`,
        null AS `product_name`,
        null AS `porduct_variant_id`,
        COALESCE(SUM(purchases.price ), 0) AS `total`,
        'unicorn' AS `main_source`,
        purchases.id  AS `unicorn_puchase_id`,
        purchase_type.name AS `unicorn_purchase_type_name`,
        purchase_status.name AS `unicorn_purchase_status_name`,
        CASE WHEN purchases.is_return_user THEN 'returning' ELSE 'new' END AS `unicorn_is_returning`

      FROM os_looker.purchases  AS purchases
      LEFT JOIN os_looker.purchase_type on purchases.type = purchase_type.id
      LEFT JOIN os_looker.users  AS users ON purchases.user_id = users.id
      LEFT JOIN os_looker.courses  AS courses ON purchases.course_id = courses.id
      LEFT JOIN os_looker.payment_links AS payment_links on  purchases.id =payment_links.purchase_id AND purchases.type = 12
      LEFT JOIN os_looker.purchase_status  AS purchase_status ON purchases.status = purchase_status.id
      WHERE
       ((users.email1 NOT LIKE '%offensive-security%' AND users.email1 NOT LIKE '%offsec%') OR users.email1 IS NULL)
       AND (purchases.purchase_payment  IS NOT NULL)
      GROUP BY 1,2,3,4,5,6,7,8,9,10,12,13,14,15,16
      HAVING NOT (COALESCE(SUM(purchases.price ), 0) IS NULL)

      UNION ALL

      SELECT
        null AS `order_id`,
        null AS `record_id`,
        1 AS `quantity`,
        null AS `orderproduct_id`,
        null AS `user_id`,
        null AS `account_id`,
        a.data_source AS `data_source`,
        a.purchase_payment_date AS `order_date`,
        null AS `product_name`,
        null AS `product_variant_id`,
        a.price AS `total`,
        'unicorn' AS `main_source`,
        a.id AS `unicorn_purchase_id`,
        a.purchase_type_name AS `unicorn_purchase_type_name`,
        purchase_status.name AS `unicorn_purchase_status_name`,
        CASE WHEN purchases.is_return_user THEN 'returning' ELSE 'new' END AS `unicorn_is_returning`
      FROM analytics.manual_deals AS a
      LEFT JOIN os_looker.purchases  AS purchases ON a.id = purchases.id
      LEFT JOIN os_looker.purchase_status  AS purchase_status ON purchases.status = purchase_status.id

      ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: order_id {
    type: number
    value_format_name: id
    sql: ${TABLE}.order_id ;;
  }

  dimension: record_id {
    type: number
    value_format_name: id
    sql: ${TABLE}.record_id ;;
  }

  measure: record_id_count {
    type: count_distinct
    value_format_name: decimal_0
    sql: ${TABLE}.record_id ;;
  }


  dimension: user_id {
    type: number
    value_format_name: id
    sql: ${TABLE}.user_id ;;
  }

  dimension: account_id {
    type: number
    value_format_name: id
    sql: ${TABLE}.account_id ;;
  }

  dimension: quantity {
    type: number
    sql: ${TABLE}.quantity ;;
  }

  measure: quantity_sum {
    type: sum
    sql: ${TABLE}.quantity ;;
  }

  dimension: data_source {
    type: string
    sql: ${TABLE}.data_source ;;
  }

  dimension_group: order_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_date ;;
  }

  dimension_group: mtd_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      week_of_year,
      day_of_week_index,
      month,
      quarter,
      year
    ]
    sql: coalesce(${latest_payment.updated_at_date},${latest_payment.payment_date} ,${order_date_date} ) ;;
  }

  dimension: product_name {
    type: string
    sql: ${TABLE}.product_name ;;
  }

  dimension: booking_type{
    type: string
    sql: case when ${data_source} = "LMS_credits" then "ARR"
              when ${data_source} = "LMS_licenses" then "non_ARR"
              when ${data_source} = "B2C" and ( ${product_name} in ( "Learn One" , "Learn Unlimited" , "Learn Fundamentals","PG Practice" )) then "ARR"
               when ${data_source} = "B2C" and NOT ( ${product_name} in ( "Learn One" , "Learn Unlimited" , "Learn Fundamentals","PG Practice" )) then "non_ARR"
               else "unknown" END;;
  }

  dimension: product_name_group {
    type: string
    sql: case -- when ${TABLE}.product_name LIKE  "%Exam%" then "Certification"
                when ${TABLE}.product_name in ( "OSCP", "OSED", "OSWP","OSEP", "OSWE", "KLCP")  then "Certification"
                -- when ${TABLE}.product_name in ( "OSCP", "OSED","OSEP", "OSWE", "KLCP")  then "Certification"
              when ${TABLE}.product_name = "Lab Extension"  then "Lab"
              else ${TABLE}.product_name END
               ;;
  }

  dimension: product_name_group_sort {
    type: string
    case: {
      when: {
        sql: ${product_name_group} = 'PEN-200' ;;
        label: "PEN-200"
      }
      when: {
        sql: ${product_name_group} = 'PEN-300' ;;
        label: "PEN-300"
      }
      when: {
        sql: ${product_name_group} = 'EXP-301' ;;
        label: "EXP-301"
      }
      when: {
        sql: ${product_name_group} = 'WEB-300' ;;
        label: "WEB-300"
      }
      when: {
        sql: ${product_name_group} = 'CTP' ;;
        label: "CTP"
      }
      when: {
        sql: ${product_name_group} = 'WiFu' ;;
        label: "WiFu"
      }
      when: {
        sql: ${product_name_group} = 'Learn One' ;;
        label: "Learn One"
      }
      when: {
        sql: ${product_name_group} = 'Learn Unlimited' ;;
        label: "Learn Unlimited"
      }
      when: {
        sql: ${product_name_group} = 'Learn Fundamentals' ;;
        label: "Learn Fundamentals"
      }
      when: {
        sql: ${product_name_group} = 'PG Practice' ;;
        label: "PG Practice"
      }
      when: {
        sql: ${product_name_group} = 'Lab' ;;
        label: "Lab"
      }
      when: {
        sql: ${product_name_group} = 'Certification' ;;
        label: "Certification"
      }
      when: {
        sql: ${product_name_group} = 'General Purchase' ;;
        label: "General Purchase"
      }
      when: {
        sql: ${product_name_group} = 'Flex' ;;
        label: "Flex"
      }
      else: "Other"
    }
  }

  dimension: total_dim {
    type: number
    sql: ${TABLE}.total ;;
  }

  dimension: orderproduct_id {
    type: number
    value_format_name: id
    sql: ${TABLE}.orderproduct_id ;;
  }

  dimension: productvariant_id {
    type: number
    value_format_name: id
    sql: ${TABLE}.productvariant_id ;;
  }


  dimension: is_returning {
    type: string
    # sql: case when ${lms_student_facts_pdt.first_paid_B2C_order_id} is NULL then  'Returning' else 'New' END ;;
    sql: case when  ${first_paid_order_pdt.first_paid_order_id} is NOT NULL OR ${TABLE}.unicorn_is_returning = 'new' then  'New' else 'Returning' END ;;
  }

  dimension: temp_is_existing {
    type: yesno
    sql: case when ${first_paid_order_pdt.first_paid_order_id} is NULL then  true else false END ;;

  }

  dimension: main_source {
    type: string
    sql: ${TABLE}.main_source ;;
  }

  dimension: unicorn_purchase_id {
    type: number
    sql: ${TABLE}.unicorn_purchase_id ;;
  }

  dimension: unicorn_purchase_type_name {
    type: string
    sql: ${TABLE}.unicorn_purchase_type_name ;;
  }

  dimension: unicorn_purchase_status_name {
    type: string
    sql: ${TABLE}.unicorn_purchase_status_name ;;
  }

  measure: total {
    type: sum
    value_format_name: usd_0
    sql: ${TABLE}.total ;;
  }

  measure: B2B_total {
    type: sum
    value_format_name: usd_0
    sql: case when ${data_source} in ("LMS_licenses", "LMS_credits") then ${TABLE}.total else 0 END
      ;;
  }

  measure: B2C_total {
    type: sum
    value_format_name: usd_0
    sql: case when ${data_source} in ("B2C") then ${TABLE}.total else 0 END
      ;;
  }

  measure: pg_practice_total {
    label: "B2C PG Practice"
    type: sum
    value_format_name: usd_0
    sql: case when ${data_source} = "B2C"  and ${product_name} = "PG Practice" then ${TABLE}.total else 0 END ;;
  }

  measure: learn_total {
    label: "B2B Learn Subs"
    type: sum
    value_format_name: usd_0
    sql: case when ${data_source} = "LMS_licenses" and ( ${product_name} = "Learn One" OR  ${product_name} = "Learn Unlimited" OR   ${product_name} = "Learn Fundamentals" ) then ${TABLE}.total else 0 END ;;
  }

  measure:B2B_learnone_total {
    label: "B2B LearnOne"
    type: sum
    value_format_name: usd_0
    sql: case when ${data_source} = "LMS_licenses" and ( ${product_name} = "Learn One" ) then ${TABLE}.total else 0 END ;;
  }

  measure:B2B_learnunlimited_total {
    label: "B2B LearnUnlimited"
    type: sum
    value_format_name: usd_0
    sql: case when ${data_source} = "LMS_licenses" and ( ${product_name} = "Learn Unlimited" ) then ${TABLE}.total else 0 END ;;
  }

  measure:B2B_learnfundamententals_total {
    label: "B2B LearnFundamentals"
    type: sum
    value_format_name: usd_0
    sql: case when ${data_source} = "LMS_licenses" and ( ${product_name} = "Learn Fundamentals" ) then ${TABLE}.total else 0 END ;;
  }

  measure: learnone_count {
    label: "B2B LearnOne Count"
    type: count_distinct
    sql: case when ${data_source} = "LMS_licenses" and ( ${product_name} = "Learn One" ) then ${TABLE}.record_id  END ;;
  }

  measure: learnunlimited_count {
    label: "B2B LearnUnlimited Count"
    type: count_distinct
    sql: case when ${data_source} = "LMS_licenses" and ( ${product_name} = "Learn Unlimited" ) then ${TABLE}.record_id  END ;;
  }

  measure: learnfundamentals_count {
    label: "B2B LearnFundamentals Count"
    type: count_distinct
    sql: case when ${data_source} = "LMS_licenses" and ( ${product_name} = "Learn Fundamentals" ) then ${TABLE}.record_id  END ;;
  }


  measure: B2C_learn_subs {
    label: "B2C LearnOne"
    type: sum
    value_format_name: usd_0
    sql: case when ${data_source} = "B2C" and ( ${product_name} = "Learn One"  ) then ${TABLE}.total else 0 END ;;
  }

  measure: B2C_learn_fund {
    label: "B2C LearnFundamentals"
    type: sum
    value_format_name: usd_0
    sql: case when ${data_source} = "B2C" and ( ${product_name} = "Learn Fundamentals"  ) then ${TABLE}.total else 0 END ;;
  }

  measure: B2C_non_sub_orders{
    label: "B2C Others"
    type: sum
    value_format_name: usd_0
    sql: case when ${data_source} = "B2C" and ( ${product_name} != "Learn One"  ) and ( ${product_name} != "Learn Fundamentals"  ) and ${product_name} != "PG Practice"  then ${TABLE}.total else 0 END ;;
  }

  measure: B2C {
    label: "B2C total (excl practice)"
    type: sum
    value_format_name: usd_0
    sql: case when ${data_source} = "B2C" and ${product_name} != "PG Practice"   then ${TABLE}.total else 0 END ;;
  }

  measure:  LMS_licenses_total {
    label: "B2B Licenses"
    type: sum
    value_format_name: usd_0
    sql: case when ${data_source} = "LMS_licenses"  and NOT ( ${product_name} = "Learn One"  OR  ${product_name} = "Learn Unlimited" OR   ${product_name} = "Learn Fundamentals") then ${TABLE}.total else 0 END ;;
  }

  measure:  LMS_credits_total {
    label: "B2B Flex"
    type: sum
    value_format_name: usd_0
    sql: case when ${data_source} = "LMS_credits" then ${TABLE}.total else 0 END ;;
  }

  measure: pg_practice_count {
    label: "B2C PG Practice Count"
    type: count_distinct
    sql: case when ${data_source} = "B2C" and ${product_name} = "PG Practice"  then ${TABLE}.order_id else NULL END ;;
  }


  measure: learn_count {
    label: "B2B Learn Count"
    type: count_distinct
    sql: case when ${data_source} = "LMS_licenses" and ( ${product_name} = "Learn One" OR  ${product_name} = "Learn Unlimited"  OR   ${product_name} = "Learn Fundamentals") then ${TABLE}.record_id  END ;;
  }

  measure: B2C_learn_Count {
    label: "B2C LearnOne Count"
    type: count_distinct
    sql: case when ${data_source} = "B2C" and ( ${product_name} = "Learn One"  ) then ${TABLE}.order_id  END ;;
  }

  measure: B2C_learnfund_Count {
    label: "B2C LearnFundamentals Count"
    type: count_distinct
    sql: case when ${data_source} = "B2C" and ( ${product_name} = "Learn Fundamentals"  ) then ${TABLE}.order_id  END ;;
  }

  measure: B2C_non_sub_count{
    label: "B2C Others count"
    type: count_distinct
    sql: case when ${data_source} = "B2C" and ( ${product_name} != "Learn One"  ) and  ( ${product_name} != "PG Practice"  ) and  ( ${product_name} != "Learn Fundamentals"  ) then ${TABLE}.order_id  END ;;
  }

  measure: B2C_count {
    label: "B2C count (excl practice)"
    type: count_distinct
    sql: case when ${data_source} = "B2C" and  ( ${product_name} != "PG Practice"  )  then ${TABLE}.order_id  END ;;
  }

  measure:  LMS_licenses_count {
    label: "B2B Licenses Count"
    type: count_distinct
    sql: case when ${data_source} = "LMS_licenses"  and NOT ( ${product_name} = "Learn One" OR  ${product_name} = "Learn Unlimited" OR  ${product_name} = "Learn Fundamentals" ) then ${TABLE}.record_id  END ;;
  }

  measure:  LMS_credits_count {
    label: "B2B Flex count"
    type: count_distinct
    sql: case when ${data_source} = "LMS_credits" then ${TABLE}.record_id  END ;;
  }

  measure:  unicorn_total_exclude_pentest {
    group_label: "unicorn measures"
    type: sum
    value_format_name: usd_0
    sql: case when ${TABLE}.data_source = 'Non_course' then 0 else ${TABLE}.total END ;;
  }

  measure:  unicorn_pentest_only {
    group_label: "unicorn measures"
    type: sum
    value_format_name: usd_0
    sql: case when ${TABLE}.unicorn_purchase_type_name = "Pentest" then ${TABLE}.total else 0 END ;;
  }

  measure:  unicorn_other_total {
    group_label: "unicorn measures"
    type: sum
    value_format_name: usd_0
    sql: case when NOT ( ${TABLE}.unicorn_purchase_type_name  in ( "Flex Program","Flex Reinvestment","Pentest","Voucher")) AND ${TABLE}.unicorn_purchase_type_name IS NOT NULL then ${TABLE}.total else 0 END ;;
  }

  measure:  unicorn_other_count {
    group_label: "unicorn measures"
    type: count_distinct
    sql: case when NOT ( ${TABLE}.unicorn_purchase_type_name  in ( "Flex Program","Flex Reinvestment","Pentest","Voucher",null)) then ${TABLE}.unicorn_purchase_id else 0 END ;;
  }

  measure: B2C_total_except_Practice {
    type: number
    sql: coalesce(${unicorn_other_total},0) + ${B2C_non_sub_orders} + ${B2C_learn_subs} + ${B2C_learn_fund} ;;

  }

  set: detail {
    fields: [order_id, user_id,account_id, data_source, order_date_time, product_name, total]
  }
}
