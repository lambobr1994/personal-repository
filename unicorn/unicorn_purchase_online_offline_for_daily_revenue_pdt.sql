view: unicorn_purchase_online_offline_for_daily_revenue_pdt {
  derived_table: {
    sql: select       purchases.id  AS `id`,
          purchase_type.name as purchase_type_name,
        case when   purchases.type = "0" then courses.shotcutname
            when   purchases.type = "19" and purchases.voucher_id = 0 and purchases.price > 0 then "PWK365(B2C)"
            when   purchases.type =  "7"  and v.pid is not NULL then "PWK365(B2B)"
            when   purchases.type =  "12" and payment_links.description = 'PWK365 upgrade' then "PWK365(UPG)"
            when  purchases.type =  "4" then "Certification"
            when   purchases.type =  "7" then "Voucher"
            when   purchases.type in (  "9","10","11") then "Proving_Grounds"
            when   purchases.type in (  "6","8","14","15") then "Updates/Upgrades (Course/Lab)"
            when   purchases.type in (  "13","17") then "Flex"
            --  when   purchases.type in (  "16") then "Academy"
            -- when   purchases.type = "1" then "LiveCourse"
            when purchases.type = "3" or purchases.type = "2"  then "Lab"
                    else "Other" end  AS `course_labs_grouped`,
            case when   purchases.type = "0" and  (purchases.voucher_id  = 0)  then false
                when   purchases.type = "0" and  NOT (purchases.voucher_id  = 0)  then true
                    else false end  AS `is_redeem`,
                    users.company  AS `company`,
        DATE(purchases.purchase_payment ) AS `purchase_payment_at`,
      --  COUNT(*) AS `purchases.count`,
        COALESCE(SUM(purchases.price ), 0) AS `price`,
      -- 'Unicorn_db' AS data_source
      case when  purchases.created_by in (2,5,6,7,8) then 'salesforce'
        else 'platform' end AS data_source
      FROM os_looker.purchases  AS purchases
      left join os_looker.purchase_type on purchases.type = purchase_type.id
      LEFT JOIN os_looker.users  AS users ON purchases.user_id = users.id
      LEFT JOIN os_looker.courses  AS courses ON purchases.course_id = courses.id
      LEFT JOIN os_looker.payment_links as payment_links on  purchases.id =payment_links.purchase_id AND purchases.type = 12
      left join (SELECT
                  purchases.id  AS `pid`
                  FROM os_looker.voucher  AS voucher
                  LEFT JOIN os_looker.purchases  AS purchases ON voucher.purchase_id = purchases.id
                  WHERE (purchases.purchase_payment  IS NOT NULL) AND (voucher.purchase_type = 19)) as v on v.pid = purchases.id and purchases.type = 7 -- Left JOIN os.voucher  AS voucher ON voucher.purchase_id = purchases.id and voucher.purchase_type =19 -- joining to PWK365(b2b)

      -- LEFT JOIN os.purchase_type  AS voucher_purchase_type ON voucher.purchase_type = voucher_purchase_type.id
      WHERE
      ((users.email1 NOT LIKE '%offensive-security%' AND users.email1 NOT LIKE '%offsec%') OR users.email1 IS NULL)
      AND (purchases.purchase_payment  IS NOT NULL)
      -- AND purchases.type = 7 and purchases.id = 845185
      --  and purchases.purchase_payment > "2020-01-01"-- WHERE
      -- (((purchases.purchase_payment ) >=
    -- ((TIMESTAMP(DATE_FORMAT(DATE(NOW()),'%Y-01-01'))))
    --  AND (purchases.purchase_payment ) < ((DATE_ADD(TIMESTAMP(DATE_FORMAT(DATE(NOW()),'%Y-01-01')),INTERVAL 1 year)))))
      GROUP BY 1,2,3,4,5,6
      HAVING
        NOT (COALESCE(SUM(purchases.price ), 0) IS NULL)
      union select * from analytics.manual_deals ;;
  }


  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: id {
    type: string
    primary_key: yes
    sql: ${TABLE}.id ;;
  }

  dimension: course_labs_grouped {
    type: string
    sql: ${TABLE}.course_labs_grouped ;;
  }

  dimension: purchase_type_name {
    type: string
    sql: ${TABLE}.purchase_type_name ;;
  }

  dimension: is_redeem {
    label: "is_voucher_redeem"
    type: yesno
    sql: ${TABLE}.is_redeem ;;
  }

  dimension: is_return_user {
    type: yesno
    sql: ${unicorn_purchases.is_return_user};;
  }

  dimension: data_source {
    label: "data_source_grp"
    type: string
    sql: case when ${TABLE}.data_source = 'Non_course' then 'Non_course'  else 'Unicorn_db' END;;
  }

  dimension: data_source_w_manual {
    label: "data_source w/ manual"
    type: string
    sql: ${TABLE}.data_source ;;
  }

  dimension: budget_type {
    type: string
    sql: case when ${TABLE}.data_source  = 'Non_course' then 'Pentest'
              when ${TABLE}.data_source = 'salesforce' then 'B2B'
              when ${TABLE}.data_source = 'platform' then 'B2C' END
    ;;
  }

  dimension: course_labs_grouped_sort {
    label: "course grouped sort"
    case: {

      when: {
        sql: ${course_labs_grouped} = 'PWK' ;;
        label: "PEN-200"
      }
      when: {
        sql: ${course_labs_grouped} = 'PWK365(B2C)' ;;
        label: "PWK365(B2C)"
      }
      when: {
        sql: ${course_labs_grouped} = 'PWK365(B2B)' ;;
        label: "PWK365(B2B)"
      }
      when: {
        sql: ${course_labs_grouped} = 'PWK365(UPG)' ;;
        label: "PWK365(UPG)"
      }
      when: {
        sql: ${course_labs_grouped} = 'PEN-300' ;;
        label: "PEN-300"
      }
      when: {
        sql: ${course_labs_grouped} = 'EXP-301' ;;
        label: "EXP-301"
      }
      when: {
        sql: ${course_labs_grouped} = 'AWAE' ;;
        label: "WEB-300"
      }
      when: {
        sql: ${course_labs_grouped} = 'CTP' ;;
        label: "CTP"
      }
      when: {
        sql: ${course_labs_grouped} = 'WiFu' ;;
        label: "WiFu"
      }
      when: {
        sql: ${course_labs_grouped} = 'Lab' ;;
        label: "Lab"
      }
      when: {
        sql: ${course_labs_grouped} = 'Proving_Grounds' ;;
        label: "Proving Grounds"
      }
      when: {
        sql: ${course_labs_grouped} = 'Lab' ;;
        label: "Lab"
      }
      when: {
        sql: ${course_labs_grouped} = 'Certification' ;;
        label: "Certification"
      }

      when: {
        sql: ${course_labs_grouped} = 'Voucher' ;;
        label: "Voucher"
      }

      when: {
        sql: ${course_labs_grouped} = 'General Purchase' ;;
        label: "General Purchase"
      }
      when: {
        sql: ${course_labs_grouped} = 'Live Training' ;;
        label: "Live Training"
      }
      when: {
        sql: ${course_labs_grouped} = 'Pentest' ;;
        label: "Pentest"
      }
      when: {
        sql: ${course_labs_grouped} = 'EDB_CVE' ;;
        label: "EDB_CVE"
      }
      when: {
        sql: ${course_labs_grouped} = 'Updates/Upgrades (Course/Lab)' ;;
        label: "Updates/Upgrades (Course/Lab)"
      }

      when: {
        sql: ${course_labs_grouped} = 'Flex' ;;
        label: "Flex"
      }

      # when: {
      #   sql: ${course_labs_grouped} = 'Academy' ;;
      #   label: "Academy"
      # }

      else: "Other"

    }
  }

  dimension: type_sort {
    label: "type_sort"
    case: {
      when: {
        sql: ${course_labs_grouped} = 'Voucher' or ${course_labs_grouped} = 'PWK365(B2B)'  ;;
        label: "Voucher"
      }

      when: {
        sql: ${course_labs_grouped} = 'Flex' ;;
        label: "Flex"
      }

      when: {
        sql: ${course_labs_grouped} = 'Pentest' ;;
        label: "Pentest"
      }




      else: "Other"

    }
  }
  dimension: company {
    type: string
    sql: ${TABLE}.company ;;
  }

#   dimension: purchase_payment {
#     type: date
#     sql: ${TABLE}.purchase_payment_at;;
#   }

  dimension_group: purchase_payment_at {
    type: time
    timeframes: [
      raw,
      date,
      time,
      hour,
      week,
      month,
      month_name,
      month_num,
      quarter,
      year
    ]
    sql: ${TABLE}.purchase_payment_at;;
  }

  dimension: price {
    type: number
    sql: ${TABLE}.price ;;
  }

  measure: price_sum {
    type: sum
    value_format_name: usd_0
    sql: ${TABLE}.price ;;
  }

  dimension: is_returning {
    type: string
    sql: case when ${is_return_user} then 'Returning' else 'New' end ;;
  }

  measure: Voucher_total {
    label: "Voucher"
    type: sum
    value_format_name: usd_0
    sql: case when ${type_sort} = "Voucher" then ${TABLE}.price else 0 END ;;
  }

  measure:  pentest_total {
    label: "Pentest"
    type: sum
    value_format_name: usd_0
    sql: case when ${type_sort} = "Pentest" then ${TABLE}.price else 0 END ;;
  }

  measure:  total_exclude_pentest {
    label: "Total_exclude_Pentest"
    type: sum
    value_format_name: usd_0
    sql: case when ${TABLE}.data_source = 'Non_course' then 0 else ${TABLE}.price END ;;
  }


  measure:  flex_total {
    label: "Flex"
    type: sum
    value_format_name: usd_0
    sql: case when ${type_sort} = "Flex" then ${TABLE}.price else 0 END ;;
  }

  measure:  other_total {
    label: "Other"
    type: sum
    value_format_name: usd_0
    sql: case when NOT ( ${type_sort}  in ( "Flex","Pentest","Voucher")) then ${TABLE}.price else 0 END ;;
  }

  measure:  other_count {
    label: "Other Count"
    type: count_distinct
    sql: case when NOT ( ${type_sort}  in ( "Flex","Pentest","Voucher")) then ${TABLE}.id END ;;
  }


  measure: purchase_count {
    type: count_distinct
    value_format_name: decimal_0
    sql: ${TABLE}.id ;;
  }

  measure:  live_training_total {
    label: "Live_Training"
    type: sum
    value_format_name: usd_0
    sql: case when ${purchase_type_name} = "Live Training" then ${TABLE}.price else 0 END ;;
  }

  set: detail {
    fields: [id,purchase_type_name,course_labs_grouped,
      is_redeem, company,  price, data_source]
  }
}
